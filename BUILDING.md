# Building Salem GDK

## Table of Contents

- [Supported Platforms](#supported-platforms/configurations)
- [Dependencies and Requirements](#dependencies-and-requirements)
  - [Finding your OpenGL Version](#finding-your-opengl-version)
- [Preparing your Linux environment](#preparing-your-linux-environment)
  - [Checking your Display Server](#checking-your-display-server)
  - [Preparing Arch Linux](#preparing-arch-linux)
- [Preparing your (MacOS/OSX) environment](#preparing-your-macos/osx-environment)
- [Preparing your Windows environment](#preparing-your-windows-environment)
  - [Building the remaining dependencies](#building-the-remaining-dependencies)
- [Building](#building)

## Supported Platforms/Configurations

The following configurations have been tested to work with Salem GDK, but others may too:

- [Linux](#preparing-your-linux-environment)
  - [Arch Linux](#preparing-arch-linux)
    - `Manjaro 17.1.6` with `Gnome 3.26.2` and `X11 1.19.6`
- [MacOS/OSX](#preparing-your-macos/osx-environment)
  - `MacOS High Sierra`
- [Windows](#preparing-your-windows-environment)
  - `10.0.16299`

## Dependencies and Requirements

- [CMake 3.8.2+](https://cmake.org)
- [brew](https://brew.sh) (if you are building from MacOS/OSX)
- [assimp 4.x+](https://github.com/assimp/assimp)
- Boost 1.66.x+
- GLFW3 3.2+
- Freetype 2
- GLAD (included, located in `salem/includes`)
- GLM (included, located in `salem/includes`)
- stb_image (included, located in `salem/includes`)
- OpenGL 3.3+ (Core Profile)

### Finding your OpenGL version

- On a Unix terminal, execute `glxinfo | grep version`.

  You should see something like the dump below.

  If your `OpenGL core profile version string` is below `3.3` your machine is either not capable of running Salem or you need to update your drivers.

```text
server glx version string: 1.4
client glx version string: 1.4
GLX version: 1.4
    Max core profile version: 4.3
    Max compat profile version: 3.0
    Max GLES1 profile version: 1.1
    Max GLES[23] profile version: 3.1
OpenGL core profile version string: 4.3 (Core Profile) Mesa 17.3.6
OpenGL core profile shading language version string: 4.30
OpenGL version string: 3.0 Mesa 17.3.6
OpenGL shading language version string: 1.30
OpenGL ES profile version string: OpenGL ES 3.1 Mesa 17.3.6
OpenGL ES profile shading language version string: OpenGL ES GLSL ES 3.10
```

## Preparing your Linux environment

### Checking your display server

To check which display server you have (X11 or Wayland) if you do not already know, run the `display-server.sh` in `scripts` directory. Remember to check the script before running it though! At the time of writing this, the contents of that script are as follows:  `loginctl show-session $(loginctl | grep $(whoami) | awk '{print $1}') -p Type | grep -Po "(?<=Type=)(.+)"`.

### Preparing Arch Linux

_**Note**: At the time of writing, you **do not need AUR** to install any of Salem's dependencies._

#### Prerequisites

- Install `cmake` `3.8.2` or higher with `sudo pacman -S community/cmake`.

#### Installing Dependencies

- Install `extra/boost` `1.66.0-1` or higher with `sudo pacman -S extra/boost`.
- Install `extra/assimp` `4.1.0-1` or higher with `sudo pacman -S extra/assimp`.
- Install `extra/freetype2` `2.9-2` or higher with `sudo pacman -S extra/freetype2`.
- Install the appropriate version of GLFW3 for your display server with either
  - `sudo pacman -S community/glfw-wayland` version `3.2.1-1` or higher for Wayland users.

    or

  - `sudo pacman -S community/glfw-x11` version `3.2.1-1` or higher for X users.

#### Notes

To check for dependencies/versions, run `pacman -Ss <name>` (`sudo` not necessary).

## Preparing your MacOS/OSX environment

### Prerequisites

- If you haven't already, install [`brew`](https://brew.sh/) available [here](https://brew.sh/).
- If you haven't already, install `cmake`, available with `brew install cmake`. 

### Installing dependencies

- Install `boost` `1.66.0` or higher with `brew install boost`.
- Install `assimp` `4.1.0` or higher with `brew install assimp`.
- Install `freetype` `2.9` or higher with `brew install freetype`.
- Install `glfw` `3.2` or higher with `brew install glfw`.

### Notes

- To check for dependencies, run `brew search <name>` or check [here](http://formulae.brew.sh/)
- To check versions, either run `brew info <name>` or check [here](http://formulae.brew.sh/)

## Preparing your Windows environment

### Prerequisites

- Install [CMake](https://cmake.org/download/) (current version is `3.11.0-rc4`).
  - If you are not accustomed to installing it, please use the `.msi` download option.
  - During installation, choose one of the two options to add CMake to the path
- Install [Nuwen's MinGW (x64 only) distribution](https://nuwen.net/mingw.html)
  - It's a self-extracting archive, we suggest extracting it to `C:\MinGW\`.
  - This comes with Boost, FreeType, and BZip2 pre-installed.
  - Add `<MinGW path>\bin` to the `PATH` ([using this guide](https://superuser.com/questions/949560/how-do-i-set-system-environment-variables-in-windows-10))

### Installing GLFW3

- Go to GLFW3's [download page](http://www.glfw.org/download.html#windows-pre-compiled-binaries) and download the pre-compiled binaries for your system
- Extract the archive, and navigate to the `include` directory.
  - Copy the contents (should just be a directory called `GLFW`) to `<MinGW path>\include`.
- In the same extract archive, navigate to the `lib-mingw-w64` directory.
  - Copy the contents (`glfw3.dll` `libglfw3.a` `libglfw3dll.a`)to `<MinGW path>\lib`.

### Building and installing assimp

- Clone [assimp](https://github.com/assimp/assimp) with `git` (e.g. `git clone https://github.com/assimp/assimp.git`)
- Execute the following:
  - `cmake CMakeLists.txt -G "MinGW Makefiles" -DBUILD_SHARED_LIBS=OFF`
  - `make` (or `make -jn` where `n` is the number of cores, e.g. `make -j4`)
  - Copy the `assimp` sub-directory from `<assimp repo>\include` to `<MinGW path>\include`.
  - Copy **only** `libassimp.a` and `libIrrXML.a` (do **not** copy `libassimp.dll.a` if it exists) from `<assimp repo>\lib` to `<MinGW path>\lib`.
    - If `libassimp.dll.a` is copied, the current `FindAssimp.cmake` script will not pick `libassimp.a` and a collision between `pthread` implementations will break `ld`.

## Building

### With CLion

**_WIP!_**

1. Open CLion and click `Open Project`.
2. Navigate to and select the project directory (typically named `salem-gdk`).
3. CLion should automatically start inspecting the project and generate makefiles.
4. In your menu bar, click `Run` and then `Build` (typically bound to `Ctrl+F9`).

If CLion never appeared to do anything, right-click the project directory and click `Reload CMake Project`. CMake should open a console and information should be dumped into it.

If you see this in it at the end you should be ready to proceed onto the next step, and if not please check that you prepared your environment correctly, and if you are still having difficult open an issue [on the repository](https://gitlab.com/oddly-doddly/salem-gdk/issues).

  ```text
  -- Configuring done
  -- Generating done
  -- Build files have been written to: /home/lodicolo/git/salem-gdk/cmake-build-debug

  [Finished]
  ```

#### With the terminal

**_WIP!_**

1. `cd /path/to/salem-gdk`
2. `mkdir -p cmake-build-debug`
3. `cd cmake-build-debug`
4. `cmake ..`
5. `make` (or `make -jn` where `n` is the number of cores, e.g. `make -j4`)
