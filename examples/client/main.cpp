//
// Created by ddodds on 10/21/17.
//

#include <iostream>
#include <boost/asio.hpp>
#include "views/controllers/MainMenuController.hpp"
#include "views/MainMenuView.hpp"

int main(int argc, char *argv[]) {
  TestClient::Controllers::MainMenuController mainMenuController;
  auto mainMenuViewPtr = std::make_unique<TestClient::Views::MainMenuView>(mainMenuController);

  Salem::System::GameLoop gameLoop;
  gameLoop.LoadView(std::move(mainMenuViewPtr));
  gameLoop.Run();

  return 0;
}