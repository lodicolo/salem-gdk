//
// Created by ddodds on 10/21/17.
//

#ifndef GAME_CLIENT_CONFIG_H
#define GAME_CLIENT_CONFIG_H

namespace Config {
static const char SERVER_PORT[] = "12012";
static const char SERVER_ADDRESS[] = "127.0.0.1";
}

#endif //GAME_CLIENT_CONFIG_H
