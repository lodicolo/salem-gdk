//
// Created by ddodds on 10/24/17.
//

#ifndef TEST_CLIENT_MAINMENUVIEW_HPP
#define TEST_CLIENT_MAINMENUVIEW_HPP

#include <salem/system/GameLoop.hpp>
#include <salem/system/interface/GameView.hpp>
#include "controllers/MainMenuController.hpp"
#include "../behaviours/TestBehaviour.hpp"

#include <salem/physics/PhysicsTypes.hpp>
#include <salem/physics/types/Transform.hpp>
#include <salem/graphics/objects/Model.hpp>
#include <salem/graphics/controls/uiButton.hpp>
#include <salem/graphics/controls/uiTextBox.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace TestClient::Views {
class MainMenuView
    : public Salem::System::Interface::GameView {
 public:
  explicit MainMenuView(Controllers::MainMenuController &p_view_controller)
      : GameView(p_view_controller) {}

  void Initialize() override {
    // Load GUI
    LoadGui("./resource/gui/views/MainMenu.json");
    // Create and bind a Camera
    Salem::Physics::Types::Transform camera_transform(
        Salem::Physics::Vector3(0.0f, 0.0f, 0.0f),
        Salem::Physics::Quaternion(),
        Salem::Physics::Vector3(1.0f, 1.0f, 1.0f));

    GameView::camera(new Salem::Graphics::Objects::Camera(camera_transform,
                                                          45.0f,
                                                          GAMELOOP_PTR->view_port_width(),
                                                          GAMELOOP_PTR->view_port_height(),
                                                          0.0f,
                                                          100.0f));

    Salem::Physics::Types::Transform transform(
        Salem::Physics::Vector3(0.0f, 0.0f, 0.0f),
        Salem::Physics::Quaternion(),
        Salem::Physics::Vector3(1.0f, 1.0f, 1.0f)
    );

    // Game Objects do not get initalized until you add them to a game_view now.
    Salem::Graphics::Objects::Model
        *model = new Salem::Graphics::Objects::Model(transform, "./resource/models/nanosuit/nanosuit.obj");
    // Mono Behaviour Initialize Functions get called during the game object's initialization
    model->AddBehaviour(new Behaviours::TestBehaviour(model->transform()));
    // Append GameObject to the game_view
    AddGameObject(model);
  }

  void Update() override {
    // Some Update Logic
    GameView::Update();
  }
};
}

#endif //TEST_CLIENT_MAINMENUVIEW_HPP