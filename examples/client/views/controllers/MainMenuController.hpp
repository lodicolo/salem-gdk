//
// Created by ddodds on 1/22/18.
//

#ifndef TEST_CLIENT_MAINMENUCONTROLLER_HPP
#define TEST_CLIENT_MAINMENUCONTROLLER_HPP

#include <salem/system/interface/ViewController.hpp>
#include <salem/physics/PhysicsTypes.hpp>
#include <salem/system/GameLoop.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace TestClient::Controllers {
class MainMenuController
    : public Salem::System::Interface::ViewController {
 public:
  void on_keypress(GLFWwindow *window, int key, int scancode, int action, int mods) override {
    if (key == GLFW_KEY_W) {
      Salem::Physics::Vector3 move_transform(0.0f, 0.0f, 1.0f);
      GAMELOOP_PTR->game_view()->camera()->transform().Translate(move_transform);
    }
    if (key == GLFW_KEY_S) {
      Salem::Physics::Vector3 move_transform(0.0f, 0.0f, -1.0f);
      GAMELOOP_PTR->game_view()->camera()->transform().Translate(move_transform);
    }
    if (key == GLFW_KEY_A) {
      Salem::Physics::Vector3 move_transform(1.0f, 0.0f, 0.0f);
      GAMELOOP_PTR->game_view()->camera()->transform().Translate(move_transform);
    }
    if (key == GLFW_KEY_D) {
      Salem::Physics::Vector3 move_transform(-1.0f, 0.0f, 0.0f);
      GAMELOOP_PTR->game_view()->camera()->transform().Translate(move_transform);
    }
  }

  void on_cursor_move(GLFWwindow *p_window, double p_xpos, double p_ypos) override {
    double x_offset = p_xpos - m_last_x;
    double y_offset = p_ypos - m_last_y;

    GAMELOOP_PTR->game_view()->camera()->transform().Rotate(0.05f * x_offset, Salem::Physics::Vector3(0.0f, 1.0f, 0.0f));
    GAMELOOP_PTR->game_view()->camera()->transform().Rotate(0.05f * y_offset, Salem::Physics::Vector3(1.0f, 0.0f, 0.0f));
    m_last_x = p_xpos;
    m_last_y = p_ypos;
  };
 private:
  double m_last_x, m_last_y = 0;
};
}

#endif //TEST_CLIENT_MAINMENUCONTROLLER_HPP
