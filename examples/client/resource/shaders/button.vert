#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormals;
layout (location = 2) in vec2 aTexCoords;

out vec2 TexCoords;

uniform mat4 projection;

uniform int spriteFrame;

void main()
{
    int spriteWidth = 64;

    gl_Position = projection * vec4(aPos, 1.0);
    vec2 tc = aTexCoords;
    tc.x = ((spriteWidth * aTexCoords.x) + (spriteWidth * spriteFrame)) / 192;
    TexCoords = tc;
}