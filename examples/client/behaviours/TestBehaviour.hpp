//
// Created by Dylan Dodds on 1/30/18.
//

#ifndef TEST_CLIENT_TESTBEHAVIOUR_HPP
#define TEST_CLIENT_TESTBEHAVIOUR_HPP

#include <salem/system/interface/Behaviour.hpp>
#include <iostream>

namespace TestClient::Behaviours {
class TestBehaviour : public Salem::System::Interface::Behaviour {
 public:
  TestBehaviour(Salem::Physics::Types::Transform p_transform)
      : Behaviour(p_transform) {}

  void Initialize() override {
    std::cout << "I am initialized. I am called when a game object is initialized" << std::endl;
  }

  void BehaviourBound() override {
    std::cout << "I am behaviour b0ound, i am called when a behaviour is bound to a game object." << std::endl;
  }

  void Update() override {
    //std::cout << "I am an Update Behaviour I am called once per Update iteration" << std::endl;
  }

  void PostUpdate() override {
    // std::cout << "I am a Post Update Behaviour I am called after every update iteration" << std::endl;
  }

  void Destroy() override {
    std::cout << "I am a destroy behaviour, I cam called on the destruction of a game object." << std::endl;
  }

  void FixedUpdate() override {
    std::cout
        << "I am a FixedUpdate Behaviour, I am called at a fixed time rate. I still need to be wired to the update loop. I do not work yet."
        << std::endl;
  }
};
}

#endif //TEST_CLIENT_TESTBEHAVIOUR_HPP
