//
// Created by ddodds on 10/20/17.
//

#ifndef GAME_SERVER_CONFIG_H
#define GAME_SERVER_CONFIG_H

#include <cstdint>

namespace Config {
static const uint16_t PORT = 12012;
};

#endif //GAME_SERVER_CONFIG_H
