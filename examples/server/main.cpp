#include <iostream>
#include <boost/asio.hpp>

#include "config.h"
#include <salem/network/Server.hpp>

int main(int argc, char *argv[]) {
  boost::asio::io_service io_service;
  Salem::Network::Server server(io_service, Config::PORT);

  io_service.run();
  return 0;
}
