//
// Created by ddodds on 11/13/17.
//

#ifndef TEST_CLIENT_SYSTEM_HELPERFUNCTIONS_HPP
#define TEST_CLIENT_SYSTEM_HELPERFUNCTIONS_HPP

#include <string>

namespace Salem::System::Tools {

/// Converts an input GLFW key code point into a UTF8 ASCII Character
inline static std::string ConvertCodePointToUTF8(int p_code_point) {
  char c[5] = {0x00, 0x00, 0x00, 0x00, 0x00};
  if (p_code_point <= 0x7F) { c[0] = p_code_point; }
  else if (p_code_point <= 0x7FF) {
    c[0] = (p_code_point >> 6) + 192;
    c[1] = (p_code_point & 63) + 128;
  }
  else if (0xd800 <= p_code_point && p_code_point <= 0xdfff) {} //invalid block of utf8
  else if (p_code_point <= 0xFFFF) {
    c[0] = (p_code_point >> 12) + 224;
    c[1] = ((p_code_point >> 6) & 63) + 128;
    c[2] = (p_code_point & 63) + 128;
  }
  else if (p_code_point <= 0x10FFFF) {
    c[0] = (p_code_point >> 18) + 240;
    c[1] = ((p_code_point >> 12) & 63) + 128;
    c[2] = ((p_code_point >> 6) & 63) + 128;
    c[3] = (p_code_point & 63) + 128;
  }
  return std::string(c);
}
}

#endif //TEST_CLIENT_SYSTEM_HELPERFUNCTIONS_HPP
