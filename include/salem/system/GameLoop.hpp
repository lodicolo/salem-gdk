//
// Created by ddodds on 10/23/17.
//

#ifndef TEST_CLIENT_GAMELOOP_HPP
#define TEST_CLIENT_GAMELOOP_HPP

#include <map>
#include <mutex>
#include <memory>
#include <string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <salem/system/interface/GameView.hpp>
#include <salem/graphics/Shader.hpp>

namespace Salem::System {

/// Render loop, Creates a window display,
/// Forwards window input to Gui and GameController Systems
class GameLoop {
 public:
  GameLoop();

  ~GameLoop();

  void Run();


  /// Loads a Game View into the game loop, begins Update() and Draw() on the Gamview
  /// @param p_gameview A Unique Pointer to the Gameview Child class to be loaded
  void LoadView(std::unique_ptr<Salem::System::Interface::GameView> p_gameview) {
    std::unique_lock<std::mutex> lock(m_game_view_mutex);
    m_game_view_ptr = std::move(p_gameview);
    m_game_view_ptr->Initialize();
  }

  /// Get a pointer to the currently loaded Game View
  Salem::System::Interface::GameView *game_view() {
    std::unique_lock<std::mutex> lock(m_game_view_mutex);
    return m_game_view_ptr.get();
  }

  /// Gets a preloaded shader by its filename (excluding its extension)
  /// Example, loading Default.vert/Default.frag would be GetShader("Default")
  Graphics::Shader *GetShader(std::string p_shader_name) {
    return m_shaders[p_shader_name];
  }

  /// Get the width of the view port
  int view_port_width() { return m_viewport_width; }

  /// Gets the height of the view port
  int view_port_height() { return m_viewport_height; }

 private:
  std::mutex m_game_view_mutex;
  GLFWwindow *m_game_window = nullptr;
  std::unique_ptr<Salem::System::Interface::GameView> m_game_view_ptr;
  int m_viewport_width, m_viewport_height;

  std::map<std::string, Graphics::Shader *> m_shaders;

  static void error_callback(int error, const char *description);

  static void keypress_callback(GLFWwindow *p_window, int p_key, int p_scancode, int p_action, int p_mods);

  static void cursor_position_callback(GLFWwindow *p_window, double p_xpos, double p_ypos);

  static void mouse_button_callback(GLFWwindow *p_window, int p_button, int p_action, int p_mods);

  static void frame_buffer_size_callback(GLFWwindow *window, int p_width, int p_height);

  static void character_mods_callback(GLFWwindow *p_window, unsigned int p_code_point, int p_mods);

  static void character_callback(GLFWwindow *p_window, unsigned int p_code_point);

  void CompileShaderPrograms();

  GLuint m_polygon_mode = GL_FILL;
};
}

#endif //TEST_CLIENT_GAMELOOP_HPP
