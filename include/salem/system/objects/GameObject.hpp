//
// Created by ddodds on 11/10/17.
//

#ifndef SALEM_SYSTEM_GAMEOBJECT_HPP
#define SALEM_SYSTEM_GAMEOBJECT_HPP

#include <salem/physics/PhysicsTypes.hpp>
#include <salem/physics/types/Transform.hpp>
#include <vector>
#include <salem/system/interface/Behaviour.hpp>

namespace Salem::System::Objects {
class GameObject {
 public:
  GameObject(Physics::Types::Transform &p_transform)
      : m_transform(p_transform), m_behaviours() {}

  GameObject(Physics::Types::Transform &p_transform, std::vector<Interface::Behaviour *> &p_behaviours)
      : m_transform(p_transform), m_behaviours(p_behaviours) {}

  ~GameObject() {
    for (Interface::Behaviour *behaviour : m_behaviours) {
      behaviour->Destroy();
      delete (behaviour);
    }
  }

  // Gets the transform of the game object
  Physics::Types::Transform &transform() { return m_transform; }

  virtual void Draw() {}

  /// Add a scripted Behaviour to the game objects.
  void AddBehaviour(Interface::Behaviour *p_behaviour) {
    p_behaviour->BehaviourBound();
    m_behaviours.push_back(p_behaviour);
  }

  /// Runs when GameObject is loaded into the scene
  virtual void Initialize() {
    for (Interface::Behaviour *behaviour : m_behaviours) {
      behaviour->Initialize();
    }
  }

  /// Runs once per game loop iteration, before Draw
  void Update() {
    for (Interface::Behaviour *behaviour : m_behaviours) {
      behaviour->Update();
    }
  }

  /// Runs once per game loop iteration, after Draw
  void PostUpdate() {
    for (Interface::Behaviour *behaviour : m_behaviours) {
      behaviour->PostUpdate();
    }
  }

  /// Runs on a fixed time loop Primarily used for physics
  // TODO: WIRE ME IN
  void FixedUpdate() {
    for (Interface::Behaviour *behaviour : m_behaviours) {
      behaviour->FixedUpdate();
    }
  }
 private:
  Physics::Types::Transform m_transform;
  std::vector<Interface::Behaviour *> m_behaviours;
};
}

#endif // Header Guard
