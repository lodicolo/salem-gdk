//
// Created by ddodds on 10/23/17.
//

#ifndef TEST_CLIENT_VIEWCONTORLLER_HPP
#define TEST_CLIENT_VIEWCONTORLLER_HPP

#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace Salem::System::Interface {
class ViewController {
 public:
  /// Event Handler for when a key is pressed
  virtual void on_keypress(GLFWwindow *window, int key, int scancode, int action, int mods) {};

  /// Event Handler for when the cursor moves
  virtual void on_cursor_move(GLFWwindow *p_window, double p_xpos, double p_ypos) {};

  /// Event Handler for when a mouse button is clicked
  virtual void on_mouse_button(GLFWwindow *p_window, int p_button, int p_action, int p_mods) {};
};
}

#endif //TEST_CLIENT_VIEWCONTORLLER_HPP
