//
// Created by ddodds on 10/23/17.
//

#ifndef TEST_CLIENT_GAMEVIEW_HPP
#define TEST_CLIENT_GAMEVIEW_HPP

#include <salem/graphics/Gui.hpp>
#include <salem/graphics/objects/Model.hpp>
#include <salem/graphics/objects/Camera.hpp>
#include <salem/graphics/interface/Drawable.hpp>
#include <salem/system/interface/ViewController.hpp>
#include <salem/graphics/interface/InterfaceControl.hpp>
#include <salem/system/objects/GameObject.hpp>

namespace Salem::System::Interface {
class GameView {
 public:
  explicit GameView(ViewController &p_view_controller, const char *p_interface_file = nullptr)
      : m_view_controller(p_view_controller), m_gui(nullptr) {}

  ~GameView() = default;

  virtual void Initialize() {};

  /// Runs once per game loop iteration, called before Draw
  virtual void Update() {
    for (System::Objects::GameObject *game_object : m_game_objects) {
      game_object->Update();
    }
  };

  /// Runs on a fixed time loop, used for Physics
  virtual void FixedUpdate() {
    for (System::Objects::GameObject *game_object : m_game_objects) {
      game_object->FixedUpdate();
    }
  };

  /// Runs once per game loop iteration, called after Draw
  virtual void PostUpdate() {
    for (System::Objects::GameObject *game_object : m_game_objects) {
      game_object->PostUpdate();
    }
  };

  /// Runs once per game loop iteration, Draws all Game Objects in the view,
  /// also Draws all UI loaded into the gui of the view
  void Draw();

  /// Loads a GUI script into the gui of the view. All Currently loaded GUI will be replaced.
  /// @param p_interface_file Path to the json gui script file to be loaded.
  void LoadGui(const char *p_interface_file) { m_gui = Graphics::Gui(p_interface_file); };

  /// Gets a reference to the currently loaded GUI
  Graphics::Gui &gui() { return m_gui; }

  /// Gets a reference to the view's Game Controller
  ViewController &view_controller() { return m_view_controller; }

  /// Returns a reference to the collection of GameObjects currently loaded.
  std::vector<System::Objects::GameObject *> &game_objects() { return m_game_objects; }

  /// Adds a game object to the game view
  void AddGameObject(System::Objects::GameObject *p_game_object) {
    p_game_object->Initialize();
    m_game_objects.push_back(p_game_object);
  }

  /// Sets the currently loaded primary camera
  void camera(Graphics::Objects::Camera *p_camera) {
    m_camera = p_camera;
  }

  /// Returns a pointer to the currently loaded primary camera
  Graphics::Objects::Camera *camera() {
    return m_camera;
  }

 private:
  void DrawInterfaceControls();
  ViewController &m_view_controller;

  Graphics::Gui m_gui;
  Graphics::Objects::Camera *m_camera = nullptr;

  std::vector<System::Objects::GameObject *> m_game_objects;
};
};

#endif //TEST_CLIENT_GAMEVIEW_HPP
