//
// Created by Dylan Dodds on 1/30/18.
//

#ifndef SALEM_SYSTEM_BEHAVIOUR_HPP
#define SALEM_SYSTEM_BEHAVIOUR_HPP

#include <salem/physics/types/Transform.hpp>

namespace Salem::System::Interface {
class Behaviour {
 public:
  explicit Behaviour(Physics::Types::Transform &p_transform)
      : m_transform(p_transform) {}

  /// Called when the owning Game Object is loaded into a game view
  virtual void Initialize() {}

  /// Called when the behaviour is loaded ioto a Game Object,
  /// Regardless if that game object has already been initialized
  virtual void BehaviourBound() {}

  /// Called once per game loop iteraction, called before Draw
  virtual void Update() {}

  /// Called once per game loop iteraction, called after Draw
  virtual void PostUpdate() {}

  /// Called on a fixed time loop
  virtual void FixedUpdate() {}

  /// Called when the game object is destructed
  virtual void Destroy() {}

  /// Gets the transform of the owning Game Object
  // TODO: Replace transform with a method to get the current game object
  Physics::Types::Transform &transform() {
    return m_transform;
  }

 private:
  Physics::Types::Transform &m_transform;
};
}

#endif // Header Guard
