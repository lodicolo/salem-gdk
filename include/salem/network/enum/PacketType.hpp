//
// Created by ddodds on 10/21/17.
//

#ifndef GAME_CLIENT_PACKETTYPE_HPP
#define GAME_CLIENT_PACKETTYPE_HPP

namespace Salem::Network {

enum class PacketType : char {
  undefined,
  ping
};
}
#endif //GAME_CLIENT_PACKETTYPE_HPP
