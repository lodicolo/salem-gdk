//
// Created by ddodds on 10/21/17.
//

#include <boost/asio.hpp>

#ifndef GAME_CLIENT_HELPERFUNCTIONS_HPP
#define GAME_CLIENT_HELPERFUNCTIONS_HPP

namespace Salem::Network::Tools {
using boost::asio::ip::tcp;

static tcp::resolver::iterator ResolveEndpoint(const char address[],
                                               const char port[],
                                               boost::asio::io_service &p_io_service) {
  tcp::resolver resolver(p_io_service);
  return resolver.resolve({address, port});
}
}

#endif // GAME_CLIENT_HELPERFUNCTIONS_HPP
