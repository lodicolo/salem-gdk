//
// Created by ddodds on 10/21/17.
//

#ifndef GAME_CLIENT_NETWORKCLIENT_HPP
#define GAME_CLIENT_NETWORKCLIENT_HPP

#include <memory>
#include <vector>
#include <boost/asio.hpp>
#include <iostream>
#include <salem/network/Packet.hpp>
#include <salem/network/PacketHeader.hpp>

namespace Salem::Network {
class Client
    : public std::enable_shared_from_this<Client> {
 public:
  explicit Client(boost::asio::ip::tcp::socket p_socket);

  ~Client() { std::cout << "ided" << std::endl; }

  Client(const Client &other) = delete; // No copying!
  Client(Client &&other)
      : m_socket(std::move(other.m_socket)),
        m_read_buffer(),
        m_temp_header_ptr(nullptr) {}

  void Connect(boost::asio::ip::tcp::resolver::iterator p_endpointIterator);

  void Send(Packet p_packet);

  void BeginRead();

 private:
  void BeginReadBody();

  boost::asio::ip::tcp::socket m_socket;
  std::vector<char> m_read_buffer;
  std::unique_ptr<PacketHeader> m_temp_header_ptr;
};
}
#endif //GAME_CLIENT_NETWORKCLIENT_HPP
