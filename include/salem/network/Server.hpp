//
// Created by ddodds on 10/16/17.
//

#ifndef GAME_CLIENT_SERVER_HPP
#define GAME_CLIENT_SERVER_HPP

#include <cstdint>
#include <boost/asio.hpp>

#include <salem/network/Client.hpp>

namespace Salem::Network {
/// I'm not even gonna begin to document network, as it will be entirely rewritten soon
/// Do not use this network library for anything practical It's far from ready.
class Server {
 public:
  Server(boost::asio::io_service &p_io_service, uint16_t p_port);
 private:
  void BeginAccept();

  uint16_t m_port;
  boost::asio::ip::tcp::acceptor m_acceptor;
  boost::asio::ip::tcp::socket m_temp_socket;

  std::vector<std::unique_ptr<Client>> m_connections;
};
}
#endif //GAME_CLIENT_SERVER_HPP


