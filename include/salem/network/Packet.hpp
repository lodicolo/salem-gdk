//
// Created by ddodds on 10/21/17.
//

#ifndef GAME_CLIENT_PACKET_HPP
#define GAME_CLIENT_PACKET_HPP

#include <vector>
#include <boost/asio.hpp>

#include <salem/network/PacketHeader.hpp>

namespace Salem::Network {
class Packet {

 public:
  Packet(PacketHeader p_packet_header, char *p_data, size_t p_data_size);

  PacketHeader GetHeader() { return m_packet_header; }

  char *header_data() { return m_data.data(); }

  char *body_data() { return m_data.data() + PacketHeader::header_size; }

 private:
  PacketHeader m_packet_header;
  std::vector<char> m_data;
};
}

#endif //GAME_CLIENT_PACKET_HPP
