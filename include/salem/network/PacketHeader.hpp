//
// Created by ddodds on 10/21/17.
//

#ifndef GAME_CLIENT_PACKETHEADER_HPP
#define GAME_CLIENT_PACKETHEADER_HPP

#include <vector>
#include <cstdint>

#include <salem/network/enum/PacketType.hpp>

namespace Salem::Network {
class PacketHeader {
 public:
  enum {
    header_size = 5
  };
  enum : uint32_t {
    max_packet_size = 4096
  };

  explicit PacketHeader(char *data);

  PacketHeader(PacketType p_packet_type, uint32_t p_data_size);

  char *data() { return m_data.data(); }

  PacketType GetType() { return (PacketType) m_data[0]; };

  uint32_t GetBodySize() { return *reinterpret_cast<uint32_t *>(m_data.data() + sizeof(PacketType)); }

 private:
  std::vector<char> m_data;
};
}
#endif //GAME_CLIENT_PACKETHEADER_HPP
