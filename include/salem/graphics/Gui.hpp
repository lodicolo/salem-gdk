//
// Created by ddodds on 12/18/17.
//

#ifndef SALEM_GRAPHICS_GUI_HPP
#define SALEM_GRAPHICS_GUI_HPP
#include <memory>
#include <map>
#include <boost/property_tree/ptree.hpp>
#include <salem/graphics/interface/InterfaceControl.hpp>
#include <salem/graphics/Font.hpp>
#include <salem/physics/PhysicsTypes.hpp>
#include <string>

namespace Salem::Graphics {
namespace Controls {
class uiContainer;
}

class Gui {
 public:
  /// The Graphics User Interface Engine, Used by Game view and GUI Container objects.
  /// Initializes and Draws 2D ui objects. Draws in an orthographical view.
  /// @param p_view_file The path to the json GUI script to be loaded,
  /// Defaults to nullptr, if left nullptr, no GUI script will be loaded
  explicit Gui(const char *p_view_file = nullptr);

  /// Called when a mouse button is clicked through the game loop, regardless of its position on the game screen
  /// @param p_button The GLFW mouse button code  of the button clicked
  /// @param p_mouse_position The position of the mouse when it was clicked
  void on_mouse_down(int p_button, Physics::Position2d p_mouse_position);

  /// Called when a mouse button is released through the game loop, regardless of its position on the game screen
  /// @param p_button The GLFW mouse button code  of the button clicked
  /// @param p_mouse_position The position of the mouse when it was released
  void on_mouse_up(int p_button, Physics::Position2d p_mouse_position);

  /// Called when the mouse moves, regarless of its position on the game screen
  /// @param p_mouse_position The position of the mouse when it was moved
  void on_mouse_move(Physics::Position2d p_mouse_position);

  /// Called when a user begins to type text through the game loop, regardless of whether the UI object is focused
  /// @param p_code_point The GLFW code point of the input key
  /// @param p_mods GLFW codes for any mod keys used with the input character (shift, ctrl, alt, etc.)
  void on_char_input(unsigned int p_code_point, int p_mods);

  /// Called when a key is pushed down, regardless of whether the UI object is focused.
  /// @param p_key GLFW key code of the key pressed
  void on_key_down(int p_key);

  /// Called when a key is released, regardless of whether the UI object is focused.
  /// @param p_key GLFW key code of the key released
  void on_key_up(int p_key);

  /// Gets a Pointer of type T from the list of loaded controls based on the input id
  /// typename T must be or be a child of Salem::Graphics::Interface::InterfaceControl
  /// @param p_id The ui object's identifier (usually defined in the "id" parameter of an object in the GUI json script)
  template<typename T>
  typename std::enable_if<std::is_base_of<Interface::InterfaceControl, T>::value, T *>::type
  FindControlById(std::string p_id) {
    for (auto const &it : m_interface_controls) {
      if (it.second->id() == p_id) {
        return reinterpret_cast<T *>(it.second);
      }
    }

    return nullptr;
  }

  /// Gets a reference to the map of the currently loaded Interface Controls
  /// Map is organized by <z-index, pointer-to-ui-control>
  std::map<std::int32_t, Graphics::Interface::InterfaceControl *> &ui_controls() {
    return m_interface_controls;
  };

  /// Loads an interface control into our UI Control map
  /// @param p_control a pointer to the control to be loaded
  /// @param p_z_index the z-index of the control, optional, defaults to 0
  void AddUiControl(Salem::Graphics::Interface::InterfaceControl *p_control, std::int32_t p_z_index = 0) {
    p_control->Initialize();
    m_interface_controls[GetNextFreeZIndex(p_z_index)] = (p_control);
  }

  /// When a Gui Script is loaded, fonts are loaded by files from the script, and stored in the GUI for caching.
  /// Grabs a fond by its defined id, usually defined on a font object in the GUI JSON Script
  /// @param p_id the identifier of the font to be loaded.
  Font *FindFontById(std::string p_id) {
    return m_fonts.at(p_id);
  }

 private:
  std::int32_t GetNextFreeZIndex(std::int32_t base_z = 0) {
    for (std::int32_t z = base_z; z < 9999; ++z) {
      if (!zIndexIsTaken(z)) {
        return z;
      }
    }

    return -1;
  }

  Physics::Vector3 ConvertColorCodeToVector3(std::string &p_color_code) {
    std::string R = p_color_code.substr(0, 2);
    std::string G = p_color_code.substr(2, 2);
    std::string B = p_color_code.substr(4, 2);

    int rValue = 0;
    int gValue = 0;
    int bValue = 0;

    std::stringstream streamR(R);
    streamR >> std::hex >> rValue;
    std::stringstream streamG(G);
    streamG >> std::hex >> gValue;
    std::stringstream streamB(B);
    streamB >> std::hex >> bValue;

    float rFloat = (float) rValue / 255.0f;
    float gFloat = (float) gValue / 255.0f;
    float bFloat = (float) bValue / 255.0f;

    return Physics::Vector3(rFloat, gFloat, bFloat);
  }

  bool zIndexIsTaken(std::int32_t p_z_index) {
    return m_interface_controls.find(p_z_index) != m_interface_controls.end();
  }

  void LoadView();
  Interface::InterfaceControl *CreateControl(std::string &p_object_type, std::string &p_object_id,
                                             boost::property_tree::ptree::value_type &p_control_data, int p_z_index);
  template<class T>
  T *GenerateControl(std::string &, boost::property_tree::ptree::value_type &, int);

  void LoadContainerChildren(boost::property_tree::ptree::value_type &, Controls::uiContainer *);

  Physics::Rectangle LoadBoxTransform(boost::property_tree::ptree::value_type &, std::int32_t);
  Graphics::Controls::ControlAttributes LoadAttributes(boost::property_tree::ptree::value_type &);
  std::vector<Objects::Texture> LoadObjectTextures(boost::property_tree::ptree::value_type &);

  const char *m_view_file;
  std::map<std::string, Graphics::Font *> m_fonts;
  std::map<std::int32_t, Graphics::Interface::InterfaceControl *> m_interface_controls;
};

// Template Specialization
template<>
Controls::uiContainer *Gui::GenerateControl<Controls::uiContainer>(std::string &,
                                                                   boost::property_tree::ptree::value_type &,
                                                                   int);
}

#endif // Header Guard
