//
// Created by ddodds on 11/11/17.
//

#ifndef SALEM_GRAPHICS_FONT_HPP
#define SALEM_GRAPHICS_FONT_HPP

#include <map>
#include <string>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <salem/graphics/Shader.hpp>

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

namespace Salem::Graphics {
struct Character {
  GLuint TextureID;
  glm::ivec2 Size;
  glm::ivec2 Bearing;
  GLuint Advance;
};

class Font {
 public:
  /// A drawable collection of characters mapped by True Type Font Files
  /// @param p_font_path the path to the true type font file to be loaded
  /// @param p_shader the shader the font will use when drawn
  explicit Font(std::string p_font_path, Shader *p_shader);

  /// Gets a map of drawable characters, organized by their UTF8 ASCII Characters
  std::map<GLchar, Character> &characters() { return m_characters; };
 private:
  std::map<GLchar, Character> m_characters;
};
}

#endif // Header Guard
