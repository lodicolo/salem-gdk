//
// Created by Dylan Dodds on 1/28/18.
//

#ifndef SALEM_GRAPHICS_GLERRORCHECK_HPP
#define SALEM_GRAPHICS_GLERRORCHECK_HPP

#include <glad/glad.h>
#include <string>
#include <iostream>

/// Checks to see if there was an error with the last made openGl Call, if there was, it prints it to the output console.
/// Takes in a macro of File and line where the method was called, juse use glCheckError();
void glCheckError_(const char *p_file, int p_line);

/// Macro shortcut to glCheckError_
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void APIENTRY glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity,
                            GLsizei length, const GLchar *message, const void *userParam);

#endif // Header Guard
