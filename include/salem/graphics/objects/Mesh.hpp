//
// Created by ddodds on 1/17/18.
//

#ifndef SALEM_GRAPHICS_MESH_HPP
#define SALEM_GRAPHICS_MESH_HPP

#include <vector>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <salem/graphics/interface/Drawable.hpp>
#include <salem/physics/types/Transform.hpp>

#include <assimp/texture.h>

namespace Salem::Graphics::Objects {

class Mesh
    : public Interface::Drawable {
 public:
  /// A 3D mesh, usually loaded from a Model object
  /// @param p_shader_name The shader the mesh will be rendered with, default to model
  /// @param p_vertices the vertices of the mesh to be drawn
  /// @param p_indices the indexes in which the vertices will be drawn
  /// @param p_textures a list of textures the mesh will use
  Mesh(Physics::Types::Transform &p_transform,
       std::string p_shader_name = "model",
       std::vector<Objects::Vertex> p_vertices = std::vector<Objects::Vertex>(),
       std::vector<unsigned int> p_indices = std::vector<unsigned int>(),
       std::vector<Objects::Texture> p_textures = std::vector<Objects::Texture>());
};
}

#endif // Header Guard
