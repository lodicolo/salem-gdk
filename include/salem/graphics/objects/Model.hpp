//
// Created by ddodds on 1/17/18.
//

#ifndef SALEM_GRAPHICS_MODEL_HPP
#define SALEM_GRAPHICS_MODEL_HPP

#include <vector>
#include <string>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <salem/graphics/Shader.hpp>
#include <salem/graphics/interface/Drawable.hpp>
#include "Mesh.hpp"

namespace Salem::Graphics::Objects {

class Model : public Interface::Drawable {
 public:
  /// A Game Object of a 3D model.
  /// @param p_transform the transform of the model in world space
  /// @param p_path The path to the model file to be loaded
  /// @param p_shader_name The shader the model will use (defaults to model, meshes get shaders from the model file)
  Model(Physics::Types::Transform &p_transform, const char *p_path, std::string p_shader_name = "model")
      : Drawable(p_transform, p_shader_name, GL_STATIC_DRAW), m_path(p_path) {}

 protected:
  /// Draws the model and all the meshed attached to it
  void Draw() override;

  /// Initializes model, called when the models game object is bound to the game view
  void Initialize() override;
 private:
  std::vector<Mesh> m_meshes;
  std::string m_directory;
  std::string m_path;

  void processNode(aiNode *p_node, const aiScene *p_scene);
  Mesh processMesh(aiMesh *p_mesh, const aiScene *p_scene);
  std::vector<Texture> m_texturesLoaded;

  std::vector<Texture> loadMaterialTextures(
      aiMaterial *p_material, aiTextureType p_textureType, std::string p_textureTypeName);
};
}

#endif // Header Guard
