//
// Created by Dylan Dodds on 1/29/18.
//

#ifndef SALEM_GRAPHICS_CAMERA_HPP
#define SALEM_GRAPHICS_CAMERA_HPP

#include <salem/system/objects/GameObject.hpp>
#include <salem/physics/types/Transform.hpp>

namespace Salem::Graphics::Objects {

class Camera : public System::Objects::GameObject {
 public:
  Camera(Physics::Types::Transform p_transform, float p_fov, float p_aspect_width, float p_aspect_height,
         float p_clip_near, float p_clip_far);

  /// Fetches the camera's perspective project, Creates a perspective call through glm, changes the
  /// projection mode to perspective for the draw.
  glm::mat4 projection() {
    return glm::perspective(glm::radians(m_fov), m_aspect_width / m_aspect_height, m_clip_near, m_clip_far);
  }

  /// I don't even think this works right anymore... Or ever did...
  // TODO: Test and fix
  void LookAt(Physics::Vector3 p_target) {
    m_target = p_target;
    transform().Rotate(glm::normalize(transform().position() - m_target));
  }

 private:
  float m_fov, m_aspect_width, m_aspect_height, m_clip_near, m_clip_far;
  Physics::Vector3 m_target;
};

};

#endif // Header Guard
