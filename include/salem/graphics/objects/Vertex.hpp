//
// Created by Dylan Dodds on 1/25/18.
//

#ifndef SALEM_GRAPHICS_VERTEX_HPP
#define SALEM_GRAPHICS_VERTEX_HPP

#include <salem/physics/PhysicsTypes.hpp>

namespace Salem::Graphics::Objects {

/// Information regarding a vertex, passed on to the Draw of OpenGl. Eventually passed off to the shader
struct Vertex {
  Physics::Vector3 Position;
  Physics::Vector3 Normal;
  Physics::Vector2 TexCoords;
};
}

#endif // Header Guard
