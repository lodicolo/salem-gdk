//
// Created by Dylan Dodds on 1/25/18.
//

#ifndef SALEM_GRAPHICS_TEXTURE_HPP
#define SALEM_GRAPHICS_TEXTURE_HPP

#include <string>

namespace Salem::Graphics::Objects {
/// Information regarding a texture loaded into opengl
struct Texture {
  unsigned int id;
  std::string type;
  std::string path;
};

/// Loads a texture into OpenGl
/// @param p_path the path to the texture to load
/// @return The OpenGl id of the texture that was loaded
unsigned int TextureFromFile(std::string &p_path);

/// Loads a texture into OpenGl
/// @param p_path the file to the texture to load
/// @param p_directory the directory of the file being loaded
/// @return The OpenGl id of the texture that was loaded
unsigned int TextureFromFile(const char *p_path, const std::string &p_directory);
}

#endif // Header Guard
