//
// Created by ddodds on 11/9/17.
//

#ifndef SALEM_GRAPHICS_SHADER_HPP
#define SALEM_GRAPHICS_SHADER_HPP

#include <glad/glad.h>
#include <glm/glm.hpp>

namespace Salem::Graphics {
class Shader {
 public:
  /// Loads and compiles a fragment and vertex shader
  Shader(const GLchar *p_vertex_path, const GLchar *p_fragment_path);

  /// Makes an openGL Call to bind the currently loaded shader
  void Use();

  /// Gets the OpenGL Shader Id of the currently loaded shader program
  GLuint id() { return m_shader_program; }

  /// Sets a uniform boolean value in the loaded value
  /// @param name The Name of uniform value to set
  /// @param value the value of the uniform we're setting
  void SetBool(const std::string &name, bool value) const;

  /// Sets a uniform Integer value in the loaded value
  /// @param name The Name of uniform value to set
  /// @param value the value of the uniform we're setting
  void SetInt(const std::string &name, int value) const;

  /// Sets a uniform Float value in the loaded value
  /// @param name The Name of uniform value to set
  /// @param value the value of the uniform we're setting
  void SetFloat(const std::string &name, float value) const;

  /// Sets a uniform Matrix4 value in the loaded value
  /// @param name The Name of uniform value to set
  /// @param value the value of the uniform we're setting
  void SetMat4(const std::string &name, glm::mat4 value) const;
 private:
  GLuint m_shader_program;
};
}

#endif  // Header Guard
