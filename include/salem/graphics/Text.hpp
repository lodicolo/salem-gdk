//
// Created by ddodds on 11/11/17.
//

#ifndef SALEM_GRAPHICS_TEXT_HPP
#define SALEM_GRAPHICS_TEXT_HPP

#include <string>

#include <salem/graphics/Font.hpp>
#include <salem/graphics/interface/Drawable2d.hpp>
#include <salem/physics/PhysicsTypes.hpp>
#include <salem/system/objects/GameObject.hpp>

namespace Salem::Graphics {
class Text
    : public Graphics::Interface::Drawable2d {
 public:

  // Todo: Remove all these extra params, use ControlProperties instead.
  /// A drawable collection of chars. Uses Font to draw its textutres
  /// @param p_box_transform The location and scale to orthographically draw
  /// @param p_text A string of text to be drawn to the screen
  /// @param p_font The font to draw with.
  /// @param p_scale The size of the font to be drawn.
  /// @parama p_color The color of the font to be drawn.
  Text(Physics::Rectangle p_box_transform,
       std::string p_text,
       Font &p_font,
       GLfloat p_scale,
       Physics::Vector3 p_color);

  void Draw() override;
  void Initialize() override;

  std::string &text() { return m_text; }

  void text(std::string &p_text) { m_text = p_text; }

 private:
  Physics::Vector3 m_color;
  GLfloat m_scale;
  std::string m_text;
  Font &m_font;
};
}

#endif  // Header Guard
