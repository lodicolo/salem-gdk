//
// Created by ddodds on 12/20/17.
//

#ifndef SALEM_GRAPHICS_CONTROLATTRIBUTES_HPP
#define SALEM_GRAPHICS_CONTROLATTRIBUTES_HPP

#include <string>
#include <salem/graphics/Font.hpp>
#include <salem/physics/PhysicsTypes.hpp>

namespace Salem::Graphics::Controls {
struct ControlAttributes {
  Font *font = nullptr;
  std::string text_align = "";
  std::string anchor = "";
  std::string place_holder = "";
  Physics::Vector3 font_color = Physics::Vector3(0.0f, 0.0f, 0.0f);
  std::string label = "";
  bool is_password = false;
  float font_size = 0.0;
};
}
#endif  // Header Guard
