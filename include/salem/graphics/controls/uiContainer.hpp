//
// Created by Dylan Dodds on 1/31/18.
//

#ifndef SALEM_GRAPHICS_UICONTAINER_HPP
#define SALEM_GRAPHICS_UICONTAINER_HPP

#include <salem/graphics/interface/InterfaceControl.hpp>
#include <salem/graphics/Gui.hpp>

namespace Salem::Graphics::Controls {
class uiContainer : public Interface::InterfaceControl {
 public:
  uiContainer(std::string p_id,
              Physics::Rectangle p_box_transform,
              std::string p_shader_name,
              Controls::ControlAttributes p_attributes = Controls::ControlAttributes())
      : InterfaceControl(p_id, p_box_transform, p_shader_name, p_attributes), m_gui() {}

  void AddControl(Interface::InterfaceControl *p_control, int p_z_index) {
    p_control->box_transform(Physics::Rectangle(
        box_transform().x + p_control->box_transform().x,
        box_transform().y + p_control->box_transform().y,
        p_control->box_transform().width,
        p_control->box_transform().height
    ));

    p_control->Initialize();
    m_gui.AddUiControl(p_control, p_z_index);
  }

  template<typename T>
  typename std::enable_if<std::is_base_of<Interface::InterfaceControl, T>::value, T *>::type
  FindChildById(std::string p_id) {
    return m_gui.FindControlById<T>(p_id);
  }

  void on_mouse_move(Physics::Position2d p_mouse_position) override {
    if (p_mouse_position.IsWithin(box_transform())) {
      m_gui.on_mouse_move(p_mouse_position);
    }
  }

  void on_mouse_down(int p_button, Physics::Position2d p_mouse_position) override {
    if (p_mouse_position.IsWithin(box_transform())) {
      m_gui.on_mouse_down(p_button, p_mouse_position);
    }
  }

  void on_mouse_up(int p_button, Physics::Position2d p_mouse_position) override {
    if (p_mouse_position.IsWithin(box_transform())) {
      m_gui.on_mouse_up(p_button, p_mouse_position);
    }
  }

  void on_key_down(int p_key) override {
    m_gui.on_key_down(p_key);
  }

  void on_key_up(int p_key) override {
    m_gui.on_key_up(p_key);
  }

  void on_char_input(unsigned int p_code_point, int p_mods) override {
    m_gui.on_char_input(p_code_point, p_mods);
  }

  void Draw() override {
    InterfaceControl::Draw();

    for (auto const &it : m_gui.ui_controls()) {
      it.second->Draw();
    }
  }

 private:
  Gui m_gui;
  std::string m_hover_control_id;
  std::string m_focus_control_id;
};
}

#endif // Header Guard
