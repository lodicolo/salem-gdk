//
// Created by ddodds on 11/13/17.
//

#ifndef SALEM_GRAPHICS_UITEXTBOX_HPP
#define SALEM_GRAPHICS_UITEXTBOX_HPP

#include <string>
#include <iostream>

#include <salem/physics/tools/HelperFunctions.hpp>
#include <salem/system/tools/HelperFunctions.hpp>

#include <salem/graphics/interface/InterfaceControl.hpp>
#include <salem/graphics/Text.hpp>
#include <salem/graphics/Font.hpp>
#include <salem/physics/PhysicsTypes.hpp>
#include <salem/system/GameLoop.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace Salem::Graphics::Controls {
class uiTextBox
    : public Interface::InterfaceControl {
 public:
  uiTextBox(std::string p_id,
            Physics::Rectangle p_box_transform,
            std::string p_shader_name = "default",
            Controls::ControlAttributes p_attributes = Controls::ControlAttributes())
      : InterfaceControl(p_id, p_box_transform, p_shader_name, p_attributes),
        m_is_password(p_attributes.is_password), m_place_holder(p_attributes.place_holder), m_value(""),
        m_value_drawable(p_box_transform, m_value,
                         *p_attributes.font, p_attributes.font_size, Physics::Vector3(1.0, 1.0, 0.0)) {
    updateDrawable();
  }

  std::string &value() { return m_value; }

  void value(std::string &p_value) { m_value = p_value; }

  void on_mouse_down(int p_button, Physics::Position2d p_mouse_position) {
    if (p_mouse_position.IsWithin(box_transform())) {
      active(true);
    } else {
      active(false);
    }
  }

  void on_key_down(int p_key) override {
    if (!m_is_active) return;

    if (p_key == GLFW_KEY_BACKSPACE && m_cursor_position > 0) {
      m_value.pop_back();
      updateDrawable();

      --m_cursor_position;
    }
  }

  void on_char_input(unsigned int p_code_point, int p_mods) override {
    if (!m_is_active) return;

    std::string utf8Char = System::Tools::ConvertCodePointToUTF8(p_code_point);
    m_value.append(utf8Char);
    updateDrawable();

    ++m_cursor_position;
  }

  void Draw() override {
    Graphics::Interface::InterfaceControl::Draw();
    m_value_drawable.Draw();
  }

  void active(bool p_value) {
    m_is_active = p_value;
  };

  bool active() {
    return m_is_active;
  };

 private:
  void updateDrawable() {
    if (m_value.length() == 0) {
      m_value_drawable.text(m_place_holder);
    } else if (m_is_password) {
      std::string passwordText;
      for (size_t i = 0; i < m_value.length(); ++i) {
        passwordText += m_password_char;
      }

      m_value_drawable.text(passwordText);
    } else {
      m_value_drawable.text(m_value);
    }
  }

  uint32_t m_cursor_position = 0;
  std::string m_value;
  std::string m_place_holder;
  Text m_value_drawable;
  bool m_is_password = false;
  char m_password_char = '*';
  bool m_is_active = false;
};
}

#endif // Header Guard
