//
// Created by ddodds on 10/24/17.
//

#ifndef SALEM_GRAPHICS_UIBUTTON_HPP
#define SALEM_GRAPHICS_UIBUTTON_HPP

#include <iostream>
#include <glad/glad.h>
#include <salem/graphics/interface/InterfaceControl.hpp>
#include <salem/graphics/interface/Drawable.hpp>
#include <salem/physics/PhysicsTypes.hpp>
#include <salem/system/objects/GameObject.hpp>
#include <salem/graphics/Text.hpp>
#include <salem/system/GameLoop.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace Salem::Graphics::Controls {
class uiButton
    : public Interface::InterfaceControl {

 public:
  uiButton(
      std::string &p_id,
      Physics::Rectangle p_box_transform,
      std::string p_shader_name,
      Controls::ControlAttributes p_attributes)
      : InterfaceControl(p_id, p_box_transform, p_shader_name, p_attributes),
        m_label_drawable(GetLabelTransform(), p_attributes.label, *p_attributes.font, p_attributes.font_size,
                         p_attributes.font_color) {}

  virtual void Draw() override {
    m_shader->Use();
    m_shader->SetInt("spriteFrame", m_sprite_frame);

    InterfaceControl::Draw();
    m_label_drawable.Draw();
  }

  virtual void on_mouse_move(Physics::Position2d p_mouse_position) override {
    if (p_mouse_position.IsWithin(box_transform())) {
      if (m_sprite_frame != 2) {
        m_sprite_frame = 1;
      }
    } else {
      m_sprite_frame = 0;
    }
  }

  virtual void on_mouse_down(int p_button, Physics::Position2d p_mouse_position) override {
    if (p_mouse_position.IsWithin(box_transform())) {
      m_sprite_frame = 2;
    }
  }

  virtual void on_mouse_up(int p_button, Physics::Position2d p_mouse_position) override {
    if (p_mouse_position.IsWithin(box_transform())) {
      m_sprite_frame = 1;
      if (on_click != nullptr) on_click();
    }

  }

  void (*on_click)() = nullptr;

 private:
  int m_sprite_frame = 0;
  Text m_label_drawable;

  Physics::Rectangle GetLabelTransform() {
    return Physics::Rectangle(
        box_transform().x + box_transform().width / 2 - (16 * attributes().label.length() * attributes().font_size),
        box_transform().y + box_transform().height / 2 - (16 * attributes().font_size), 0.0f, 0.0f);
  }
};
}

#endif // Header Guard
