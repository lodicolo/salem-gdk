//
// Created by ddodds on 12/20/17.
//

#ifndef SALEM_GRAPHICS_UIIMAGE_HPP
#define SALEM_GRAPHICS_UIIMAGE_HPP

#include <string>
#include <salem/physics/PhysicsTypes.hpp>
#include <salem/graphics/interface/InterfaceControl.hpp>

namespace Salem::Graphics::Controls {
class uiImage
    : public Interface::InterfaceControl {
 public:
  uiImage(std::string p_id,
          Physics::Rectangle p_box_transform,
          std::string p_shader_name = "default",
          Controls::ControlAttributes p_attributes = Controls::ControlAttributes())
      : InterfaceControl(p_id, p_box_transform, p_shader_name, p_attributes) {}
};
}

#endif // Header Guard
