//
// Created by Dylan Dodds on 2/2/18.
//

#ifndef TEST_CLIENT_DRAWABLE2D_HPP
#define TEST_CLIENT_DRAWABLE2D_HPP

#include <string>
#include <vector>
#include <glad/glad.h>

#include <salem/physics/PhysicsTypes.hpp>
#include <salem/graphics/controls/ControlAttributes.hpp>
#include <salem/graphics/objects/Vertex.hpp>
#include <salem/graphics/objects/Texture.hpp>

namespace Salem::Graphics::Interface {
class Drawable2d {
 public:
  /// A renderable object drawn in a 2D orthographical perspective
  /// @param p_box_transform A 2 dimensional transform defining where the drawable should be drawn in pixel sapce
  /// @param p_shader_name The name of the shader the drawable will use when drawn
  Drawable2d(Physics::Rectangle p_box_transform, std::string p_shader_name);

  /// Gets the 2D transform of the drawable object in pixel space
  Physics::Rectangle box_transform() {
    return m_box_transform;
  }

  /// sets the 2D transform of the drawable object in pixel space
  void box_transform(Physics::Rectangle p_rectangle) {
    m_box_transform = p_rectangle;
  }

  /// Initializes the drawable, called when a 2D object is bound to a GUI controller
  virtual void Initialize();

  /// Called during draw iteration to render the current 2D Drawable
  virtual void Draw();

  /// Gets A pointer to the shader the Drawable will use when drawn
  Graphics::Shader *shader() { return m_shader; }

  /// Sets the collection of textures the Drawable will used when drawn.
  void textures(std::vector<Objects::Texture> &p_textures) { m_textures = p_textures; }

  /// Gets a reference to the collection of texture the Drawable will used when drawn
  std::vector<Objects::Texture> &textures() { return textures(); }
 private:
  Physics::Rectangle m_box_transform;

 protected:
  std::vector<Objects::Vertex> m_vertices;
  std::vector<Objects::Texture> m_textures;
  std::vector<unsigned int> m_indices;

  GLuint m_vao, m_vbo, m_ebo = 0;
  const GLuint m_draw_method = GL_STATIC_DRAW;

  Graphics::Shader *m_shader;
  std::string m_shader_name;
};
}

#endif //TEST_CLIENT_DRAWABLE2D_HPP
