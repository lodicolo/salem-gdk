//
// Created by ddodds on 10/24/17.
//

#ifndef SALEM_GRAPHICS_INTERFACECONTROL_HPP
#define SALEM_GRAPHICS_INTERFACECONTROL_HPP

#include <iostream>

#include <salem/graphics/controls/ControlAttributes.hpp>
#include <salem/graphics/interface/Drawable2d.hpp>
#include <salem/physics/types/Transform.hpp>
#include <salem/physics/PhysicsTypes.hpp>

namespace Salem::Graphics::Interface {
class InterfaceControl
    : public Drawable2d {
 public:
  /// A 2D renderable which hold handlers for user input. Parent interface adapted by Controls
  /// @param p_id The id of the interface control, usually defined in the gui json script
  /// @param p_box_transform a 2D rectangle to render the UI Control in
  /// @param p_shader_name The name of the shader the Control will use when rendered
  /// @param p_attributes A series of paramaters used to determine stylistic render properties
  InterfaceControl(std::string p_id, Physics::Rectangle p_box_transform,
                   std::string p_shader_name, Controls::ControlAttributes p_attributes)
      : Drawable2d(p_box_transform, p_shader_name), m_id(p_id), m_attributes(p_attributes) {}

  /// Gets the Id of the control, usually set in the GUI JSON script
  std::string id() { return m_id; }

  /// Gets properties used to determine how the control will be rendered
  Salem::Graphics::Controls::ControlAttributes &attributes() { return m_attributes; };

  /// An event called when a mouse button is clicked down.
  /// @param p_button The GLFW code of mouse button clicked
  /// @param p_mouse_position x and y coordinates of where the mouse is in pixelspace
  virtual void on_mouse_down(int p_button, Physics::Position2d p_mouse_position) {}

  /// An event called when a mouse button is released.
  /// @param p_button The GLFW code of mouse button released
  /// @param p_mouse_position x and y coordinates of where the mouse is in pixelspace
  virtual void on_mouse_up(int p_button, Physics::Position2d p_mouse_position) {}

  /// An event called when a mouse button is clicked down.
  /// @param p_mouse_position x and y coordinates of where the mouse is in pixelspace
  virtual void on_mouse_move(Physics::Position2d p_mouse_position) {}

  /// An event called when a keyboard key is pressed down
  /// @param p_key The GLFW key code of the key pressed
  virtual void on_key_down(int p_key) {}

  /// An event called when a keyboard key is released
  /// @param p_key The GLFW key code of the key released
  virtual void on_key_up(int p_key) {}

  /// An event called when a user enters in text input
  /// @param p_code_point The GLFW code point of the UTF8 Character input
  /// @param p_mods GLFW codes to any mod keys used with the text input (Shift, Ctrl, Alt, etc.)
  virtual void on_char_input(unsigned int p_code_point, int p_modes) {}

 private:
  std::string m_id;
  Salem::Graphics::Controls::ControlAttributes m_attributes;
};
}

#endif // Header Guard
