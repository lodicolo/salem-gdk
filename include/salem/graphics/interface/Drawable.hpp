//
// Created by ddodds on 10/30/17.
//

#ifndef SALEM_GRAPHICS_DRAWABLE_HPP
#define SALEM_GRAPHICS_DRAWABLE_HPP

#include <vector>
#include <string>
#include <glad/glad.h>
#include <salem/system/objects/GameObject.hpp>

#include <salem/physics/PhysicsTypes.hpp>
#include <salem/physics/types/Transform.hpp>
#include <salem/graphics/Shader.hpp>
#include <salem/graphics/objects/Texture.hpp>
#include <salem/graphics/objects/Vertex.hpp>

namespace Salem::Graphics::Interface {
class Drawable
    : public System::Objects::GameObject {
 public:
  /// A 3Dimensional Drawable Object
  /// @param p_transform the transform of the object in world space
  /// @param p_shader_name the name of the shader the drawable will use when Drawn
  /// @param p_draw_method the OpenGl method that will be used to draw the drawable
  Drawable(Physics::Types::Transform p_transform, std::string p_shader_name, GLuint p_draw_method);

  /// Called when the owning game object is bound to a game veiw
  void Initialize() override;

  /// Draws the Drawable each Draw Iteration
  void Draw() override;

  /// Gets a reference to the list of textures to be used by the drawable when drawn
  std::vector<Objects::Texture> &textures() { return m_textures; }

  /// Sets the list of textures to be used by the drawable when drawn
  void textures(std::vector<Objects::Texture> p_textures) { m_textures = p_textures; }

  /// Get a pointer to the shader the drawable will used when drawn
  Graphics::Shader *shader() { return m_shader; };

  /// Gets the name of the shader the drawable will used when drawn
  std::string shader_name() { return m_shader_name; }

  /// Gets a reference to the list of vertices within our drawable
  std::vector<Objects::Vertex> &vertices() { return m_vertices; }

  /// Sets a list of vertices to be drawn
  void vertices(std::vector<Objects::Vertex> p_vertices) { m_vertices = p_vertices; }

  /// Gets a reference to our list of indices
  std::vector<unsigned int>& indices() { return m_indices; }

  /// Sets our list of indices
  void indices(std::vector<unsigned int> p_indices) { m_indices = p_indices; }

 protected:
  std::vector<Objects::Vertex> m_vertices;
  std::vector<Objects::Texture> m_textures;
  std::vector<unsigned int> m_indices;

  GLuint m_vao, m_vbo, m_ebo = 0;
  GLuint m_draw_method;

  Graphics::Shader *m_shader;
  std::string m_shader_name;
};
}

#endif // Header Guard
