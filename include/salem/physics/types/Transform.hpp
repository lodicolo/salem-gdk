//
// Created by ddodds on 1/22/18.
//

#ifndef TEST_CLIENT_TRANSFORM_HPP
#define TEST_CLIENT_TRANSFORM_HPP

#include <glm/glm.hpp>
#include <salem/physics/PhysicsTypes.hpp>

namespace Salem::Physics::Types {
class Transform {

  /// A collection if information pertaining to Position, Rotation, and Scale
  /// of an object somewhere in 3-Dimensional World Space. Feeds Data how the object will be transformed before it is drawn.
  /// May also perform transforms operations.
  ///
  /// @param p_position The position in 3D space
  /// @param Quaternion The rotation in 3D space
  /// @param the scale in 3D space
 public:

  /// TODO: Fix the transform methods. Make getters and setters fetch and set information from the matrix transform
  Transform(
      Physics::Vector3 p_position = Physics::Vector3(0.0f, 0.0f, 0.0f),
      Physics::Quaternion p_rotation = Physics::Quaternion(0.0f, 0.0f, 0.0f, 0.0f),
      Physics::Vector3 p_scale = Physics::Vector3(1.0f, 1.0f, 1.0f))
      : m_position(), m_rotation(), m_scale(p_scale), m_matrix_transform(1.0f) {
    Translate(p_position);
    Rotate(p_rotation.x, Physics::Vector3(1.0f, 0.0f, 0.0f));
    Rotate(p_rotation.y, Physics::Vector3(0.0f, 1.0f, 0.0f));
    Rotate(p_rotation.z, Physics::Vector3(0.0f, 0.0f, 1.0f));
  }

  // TODO: Fetch value from p_matrix_transform
  /// gets the position of the transform
  Physics::Vector3 position() { return m_position; }
  void position(Physics::Vector3 p_position) { m_position = p_position; }

  // TODO: Fetch value from p_matrix_transform
  /// gets the rotation of the transform
  Physics::Quaternion rotation() { return m_rotation; }

  // TODO: Fetch value from p_matrix_transform
  /// gets the scale of the transform
  Physics::Vector3 scale() { return m_scale; }

  /// Move the transform
  /// @param p_translation The vector to move your transform on
  inline void Translate(Physics::Vector3 p_translation) {
    m_matrix_transform = glm::translate(m_matrix_transform, p_translation);
    m_position += p_translation;
  }

  /// Rotates the transform.
  /// @param p_degree The degree to rotate
  /// @param p_degree The axis to rotate on
  inline void Rotate(float p_degree, Physics::Vector3 p_axis) {
    m_matrix_transform = glm::rotate(m_matrix_transform, p_degree, p_axis);
  }

  /// Rotates the transform on all axis
  /// @value p_rotation The degrees in which to rotate each axis.
  /// Example: rotate x by 5.0deg = Rotate(Physics::Vector3(5.0f, 0.0f, 0.0f));
  inline void Rotate(Physics::Vector3 p_rotation) {
    Rotate(p_rotation.x, Physics::Vector3(1.0, 0.0, 0.0));
    Rotate(p_rotation.y, Physics::Vector3(0.0, 1.0, 0.0));
    Rotate(p_rotation.z, Physics::Vector3(0.0, 0.0, 1.0));
  }

  void Scale(Physics::Vector3 p_scale) {
    m_matrix_transform = glm::scale(m_matrix_transform, p_scale);
  }

  glm::mat4 &matrix_transform() { return m_matrix_transform; }

 private:
  glm::vec3 m_position;
  glm::vec3 m_scale;
  glm::vec4 m_rotation;
  glm::mat4 m_matrix_transform; // Used for calculations
};
}

#endif //TEST_CLIENT_TRANSFORM_HPP
