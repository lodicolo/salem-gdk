//
// Created by ddodds on 10/24/17.
//

#ifndef TEST_CLIENT_PHYSICTYPES_HPP
#define TEST_CLIENT_PHYSICTYPES_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Salem::Physics {
typedef glm::vec2 Vector2;
typedef glm::vec3 Vector3;
typedef glm::vec4 Quaternion;

class Rectangle {
 public:
  Rectangle(double p_x, double p_y, double p_width, double p_height)
      : x(p_x), y(p_y), width(p_width), height(p_height) {}

  double x;
  double y;
  double width;
  double height;
};

class Position2d {
 public:
  Position2d(double p_x, double p_y) : x(p_x), y(p_y) {}

  double x;
  double y;

  /// Checks that the current point is within a given 2D Rectangle.
  // TODO: Convert to a helper method, change Position2D to a glm::vec2
  inline bool IsWithin(Rectangle rect) {
    return (
        x > rect.x &&
            y > rect.y &&
            x < rect.x + rect.width &&
            y < rect.y + rect.height
    );
  }
};
}
#endif //TEST_CLIENT_PHYSICTYPES_HPP
