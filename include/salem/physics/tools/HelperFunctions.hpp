//
// Created by ddodds on 10/24/17.
//

#ifndef TEST_CLIENT_PHYSICS_HELPERFUNCTIONS_HPP
#define TEST_CLIENT_PHYSICS_HELPERFUNCTIONS_HPP

#include <iostream>
#include <GLFW/glfw3.h>
#include <salem/physics/PhysicsTypes.hpp>

namespace Salem::Physics::Tools {

/// Convert 2D pixel coordinates into 3D world space
/// @param p_2dLocation the x and y pixel coordinates to convert
/// @param p_viewport_size The 2D pixel size of the window's view port
inline static Physics::Position2d
Convert2dCoordsTo3dSpace(Physics::Position2d p_2dLocation, Physics::Position2d p_viewport_size) {
  return Physics::Position2d(2 * (0.5 - (p_2dLocation.x / p_viewport_size.x)) * -1,
                             2 * (0.5 - (p_2dLocation.y / p_viewport_size.y)));
}

/// Converts an input 3D coordinate into a 2D Pixel Space
/// @param p_3d_location The X and Y coordinates of the 3d space to be converted
/// @param p_viewport_size The 2D pixel size of the window's view port
inline static Physics::Vector2
Convert3dCoordsTo2DSpace(Physics::Position2d p_3d_location, Physics::Position2d p_viewport_size) {
  return Physics::Vector2(((p_3d_location.x / 2) + 0.5) * p_viewport_size.x,
                          ((p_3d_location.y / 2) + 0.5) * p_viewport_size.y);
}
}

#endif //TEST_CLIENT_PHYSICS_HELPERFUNCTIONS_HPP
