set(TARGET_INCLUDE_ROOT ${PROJECT_INCLUDE_ROOT}/system)
set(TARGET_SOURCE_ROOT ${PROJECT_SOURCE_ROOT}/system)

set_relative(TARGET_INCLUDES ${TARGET_INCLUDE_ROOT}
             interface/Behaviour.hpp
             interface/GameView.hpp
             interface/ViewController.hpp
             objects/GameObject.hpp
             tools/HelperFunctions.hpp
             GameLoop.hpp
             )

set_relative(TARGET_SOURCES ${TARGET_SOURCE_ROOT}
             interface/GameView.cpp
             GameLoop.cpp
             )

set(TARGET_SOURCES
    ${PROJECT_SOURCE_ROOT}/globals.cpp
    ${TARGET_SOURCES}
    )

source_group("system" FILES ${TARGET_INCLUDES} ${TARGET_SOURCES})

set(TARGET_COMPILE_FLAGS "")
set(TARGET_EXTERNAL_LIBS "")
if (SALEM_PLATFORM_LINUX)
    set(TARGET_EXTERNAL_LIBS "-lpthread")
elseif(SALEM_PLATFORM_MAC)
elseif(SALEM_PLATFORM_WINDOWS)
endif()

# Configure Boost
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREAD ON)
set(Boost_DEBUG OFF)

# Find Boost
find_package(Boost 1.66.0 REQUIRED system filesystem)
message(STATUS "Boost_INCLUDE_DIR: ${Boost_INCLUDE_DIR}")
message(STATUS "Boost_LIBRARIES: ${Boost_LIBRARIES}")

# TODO: Remove this once system no longer is dependent on graphics
# Find Freetype2
find_package(Freetype REQUIRED)
message(STATUS "FREETYPE_INCLUDE_DIRS: ${FREETYPE_INCLUDE_DIRS}")
message(STATUS "FREETYPE_LIBRARIES: ${FREETYPE_LIBRARIES}")

set(TARGET_EXTERNAL_LIBS ${Boost_LIBRARIES} ${TARGET_EXTERNAL_LIBS})

salem_add_library(system
                  SOURCES ${TARGET_INCLUDES} ${TARGET_SOURCES}
                  INCLUDES ${Boost_INCLUDE_DIR}
                           ${FREETYPE_INCLUDE_DIRS} # TODO: Remove this (freetype) once system no longer is dependent on graphics
                  COMPILE_FLAGS ${TARGET_COMPILE_FLAGS}
                  EXTERNAL_LIBS ${TARGET_EXTERNAL_LIBS}
                  )
