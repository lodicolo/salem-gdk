//
// Created by ddodds on 10/30/17.
//

#include <salem/system/interface/GameView.hpp>
#include <salem/system/GameLoop.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace Salem::System::Interface {

void GameView::Draw() {
  for (int i = 0; i < m_game_objects.size(); ++i) {
    m_game_objects[i]->Draw();
  }

  DrawInterfaceControls();
}

void GameView::DrawInterfaceControls() {
  for (auto const &it : gui().ui_controls()) {
    it.second->Draw();
  }
}
}