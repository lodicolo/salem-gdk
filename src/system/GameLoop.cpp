//
// Created by ddodds on 10/23/17.
//

#include <salem/system/GameLoop.hpp>
#include <salem/physics/tools/HelperFunctions.hpp>

#include <memory>
#include <string>
#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <boost/filesystem.hpp>
#include <boost/filesystem/string_file.hpp>
#include <boost/range/adaptor/reversed.hpp>

#include <salem/physics/PhysicsTypes.hpp>
#include <salem/graphics/tools/glCheckError.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace Salem::System {
GameLoop::GameLoop()
    : m_game_window(nullptr) {

  if (!glfwInit()) {
    std::cout << "glfw not inited" << std::endl;
  }

  GAMELOOP_PTR = this;

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  glfwWindowHint(GLFW_FLOATING, GL_TRUE);
  //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // For OSX
#endif

  glfwSetErrorCallback(error_callback);
  m_viewport_width = 800;
  m_viewport_height = 600;
  m_game_window = glfwCreateWindow(m_viewport_width,
                                   m_viewport_height,
                                   "TestClient | Salem's World of Witchcraft",
                                   nullptr,
                                   nullptr);

  if (!m_game_window) {
    std::cout << "window creation failed" << std::endl;
    glfwTerminate();
  }

  glfwMakeContextCurrent(m_game_window);
  if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
    std::cout << "failed to initialize glad" << std::endl;
    glfwTerminate();
  }

  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

  glViewport(0, 0, 800, 600);
  glCheckError();
  auto version = (char *) glGetString(GL_VERSION);
  std::cout << version << std::endl;;
  if (!GLAD_GL_VERSION_3_3) {
    std::cout << "A minimal version of opengl 3.3 is required" << std::endl;
    glfwTerminate();
  }

  GLint flags;
  glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
  glCheckError();

  if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
    glDebugMessageCallback(glDebugOutput,
                           nullptr); // If this line fails, take it out. It's not supported on mac so i don't know of debug messages work or not.
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
  }

  CompileShaderPrograms();

  glfwSetWindowUserPointer(m_game_window, this);
  //glfwSwapInterval(1);

  glfwSetFramebufferSizeCallback(m_game_window, frame_buffer_size_callback);
  glfwSetKeyCallback(m_game_window, keypress_callback);
  glfwSetCharCallback(m_game_window, character_callback);
  glfwSetCharModsCallback(m_game_window, character_mods_callback);
  glfwSetMouseButtonCallback(m_game_window, mouse_button_callback);
  glfwSetCursorPosCallback(m_game_window, cursor_position_callback);

  glCheckError();
}

GameLoop::~GameLoop() {
  glfwDestroyWindow(m_game_window);
  glfwTerminate();
}

void GameLoop::CompileShaderPrograms() {
  boost::filesystem::path targetDirectory("./resource/shaders/");
  boost::filesystem::directory_iterator end;

  for (boost::filesystem::directory_iterator iter(targetDirectory); iter != end; ++iter) {
    if (boost::filesystem::is_regular_file(iter->path())) {
      std::string pathname = iter->path().string();
      std::string filename = iter->path().filename().string();
      if (filename.find(".vert") != std::string::npos) {
        std::cout << "shader found: " << iter->path() << std::endl;
        std::string strippedPathname = pathname.substr(0, pathname.size() - 5); // remove ".vert"
        std::string shaderName = filename.substr(0, filename.size() - 5); // remove ".vert"

        auto shader = new Graphics::Shader((strippedPathname + ".vert").c_str(),
                                           (strippedPathname + ".frag").c_str());
        m_shaders.insert(std::make_pair(shaderName, shader));
      }
    }
  }
}

void GameLoop::Run() {
  glEnable(GL_BLEND);
  glCheckError();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glCheckError();
  glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
  glCheckError();

  while (!glfwWindowShouldClose(m_game_window)) {
    if (game_view() != nullptr) {
      glClear(GL_COLOR_BUFFER_BIT);
      glCheckError();

      game_view()->Update();
      glPolygonMode(GL_FRONT_AND_BACK, m_polygon_mode);
      glCheckError();
      m_game_view_ptr->Draw();

      glfwSwapBuffers(m_game_window);
      game_view()->PostUpdate();
    }

    glfwPollEvents();
  }
}

void GameLoop::error_callback(int error, const char *description) {
  fprintf(stderr, "Error: %s\n", description);
}

// thread safe us
void GameLoop::keypress_callback(GLFWwindow *p_window, int p_key, int p_scancode, int p_action, int p_mods) {
  auto game_loop = reinterpret_cast<GameLoop *>(glfwGetWindowUserPointer(p_window));
  if (p_action == 1 || p_action == 2) {
    game_loop->game_view()->gui().on_key_down(p_key);
  } else if (p_action == 0) {
    game_loop->game_view()->gui().on_key_up(p_key);
  }

  game_loop->game_view()->view_controller().on_keypress(p_window, p_key, p_scancode, p_action, p_mods);
}

void GameLoop::cursor_position_callback(GLFWwindow *p_window, double p_xpos, double p_ypos) {
  auto game_loop = reinterpret_cast<GameLoop *>(glfwGetWindowUserPointer(p_window));

  Salem::Physics::Position2d mouse_position(p_xpos, p_ypos);
  glfwGetCursorPos(p_window, &mouse_position.x, &mouse_position.y);
  // invert mouse y cuz opengl is backwards or something like that
  mouse_position.y = -(mouse_position.y - game_loop->view_port_height());

  game_loop->game_view()->gui().on_mouse_move(mouse_position);
  game_loop->game_view()->view_controller().on_cursor_move(p_window, p_xpos, p_ypos);
}

void GameLoop::character_callback(GLFWwindow *p_window, unsigned int p_code_point) {
  GameLoop::character_mods_callback(p_window, p_code_point, 0);
}

void GameLoop::character_mods_callback(GLFWwindow *p_window, unsigned int p_code_point, int p_mods) {
  auto game_loop = reinterpret_cast<GameLoop *>(glfwGetWindowUserPointer(p_window));
  game_loop->game_view()->gui().on_char_input(p_code_point, p_mods);
}

void GameLoop::mouse_button_callback(GLFWwindow *p_window, int p_button, int p_action, int p_mods) {
  auto game_loop = reinterpret_cast<GameLoop *>(glfwGetWindowUserPointer(p_window));

  Salem::Physics::Position2d mouse_position(0, 0);
  glfwGetCursorPos(p_window, &mouse_position.x, &mouse_position.y);
  // invert mouse y cuz opengl is backwards or something like that
  mouse_position.y = -(mouse_position.y - game_loop->view_port_height());

  if (p_action == GLFW_PRESS) {
    game_loop->game_view()->gui().on_mouse_down(p_button, mouse_position);
  } else if (p_action == GLFW_RELEASE) {
    game_loop->game_view()->gui().on_mouse_up(p_button, mouse_position);
  }

  game_loop->game_view()->view_controller().on_mouse_button(p_window, p_button, p_action, p_mods);
}

void GameLoop::frame_buffer_size_callback(GLFWwindow *p_window, int p_width, int p_height) {
  auto gameLoop = reinterpret_cast<GameLoop *>(glfwGetWindowUserPointer(p_window));

  gameLoop->m_viewport_width = p_width;
  gameLoop->m_viewport_height = p_height;
  glViewport(0, 0, p_width, p_height);
  glCheckError();
}
}

// This block of comments is all the code my cat wrote in this project:
// ;'////////////////////////////////.........sswtyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyysss..............................................................'
// aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
// vc e srRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (continued on next line)
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (continued on next line)
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (continued on next line)
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (continued on next line)
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (continued on next line)
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (continued on next line)
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
//
// ( fin. - (my cat)Salem, 2k17(c) All Rights Reserved... to my cat...