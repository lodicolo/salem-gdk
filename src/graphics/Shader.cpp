//
// Created by ddodds on 11/9/17.
//

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <salem/graphics/Shader.hpp>
#include <salem/graphics/tools/glCheckError.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Salem::Graphics {
Shader::Shader(const GLchar *p_vertex_path, const GLchar *p_fragment_path) {

  GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
  GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
  std::ifstream vertexFileStream;
  std::ifstream fragmentFileStream;
  std::string vertShaderStr;
  std::string fragShaderStr;

  vertexFileStream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  fragmentFileStream.exceptions(std::ifstream::failbit | std::ifstream::badbit);

  try {
    vertexFileStream.open(p_vertex_path);
    fragmentFileStream.open(p_fragment_path);

    std::stringstream vertStream, fragStream;
    vertStream << vertexFileStream.rdbuf();
    fragStream << fragmentFileStream.rdbuf();

    vertexFileStream.close();
    fragmentFileStream.close();

    vertShaderStr = vertStream.str();
    fragShaderStr = fragStream.str();
  }
  catch (std::ifstream::failure err) {
    std::cout << "COULD NOT READ SHADER FILE" << std::endl;
  }

  const char *vertShaderSrc = vertShaderStr.c_str();
  const char *fragShaderSrc = fragShaderStr.c_str();

  GLuint vertex, fragment;
  GLint success;
  char infoLog[512];

  vertex = glCreateShader(GL_VERTEX_SHADER);
  glCheckError();
  glShaderSource(vertex, 1, &vertShaderSrc, NULL);
  glCheckError();
  glCompileShader(vertex);
  glCheckError();

  glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
  glCheckError();
  if (!success) {
    glGetShaderInfoLog(vertex, 512, NULL, infoLog);
    glCheckError();
    std::cout << "Vertex Shader Compilation Failed\n" << infoLog << std::endl;
  }

  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glCheckError();
  glShaderSource(fragment, 1, &fragShaderSrc, NULL);
  glCheckError();
  glCompileShader(fragment);
  glCheckError();

  glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
  glCheckError();

  if (!success) {
    glGetShaderInfoLog(vertex, 512, NULL, infoLog);
    glCheckError();
    std::cout << "Fragment Shader Compilation Failed\n" << infoLog << std::endl;
  }

  m_shader_program = glCreateProgram();
  glCheckError();
  glAttachShader(m_shader_program, vertex);
  glCheckError();
  glAttachShader(m_shader_program, fragment);
  glCheckError();
  glLinkProgram(m_shader_program);
  glCheckError();

  glGetProgramiv(m_shader_program, GL_LINK_STATUS, &success);
  glCheckError();
  if (!success) {
    glGetProgramInfoLog(m_shader_program, 512, NULL, infoLog);
    glCheckError();
    std::cout << "Could not link shader;\n" << infoLog << std::endl;
  }

  glDeleteShader(vertex);
  glCheckError();
  glDeleteShader(fragment);
  glCheckError();
}

void Shader::Use() {
  glUseProgram(m_shader_program);
  glCheckError();
}

void Shader::SetBool(const std::string &name, bool value) const {
  glUniform1i(glGetUniformLocation(m_shader_program, name.c_str()), (int) value);
  glCheckError();
}

void Shader::SetInt(const std::string &name, int value) const {
  glUniform1i(glGetUniformLocation(m_shader_program, name.c_str()), value);
  glCheckError();
}

void Shader::SetFloat(const std::string &name, float value) const {
  glUniform1f(glGetUniformLocation(m_shader_program, name.c_str()), value);
  glCheckError();
}

void Shader::SetMat4(const std::string &name, glm::mat4 value) const {
  glUniformMatrix4fv(glGetUniformLocation(m_shader_program, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
  glCheckError();
}
}