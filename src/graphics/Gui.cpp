//
// Created by ddodds on 12/18/17.
//

#include <salem/graphics/Gui.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <salem/graphics/controls/uiButton.hpp>
#include <salem/graphics/controls/uiImage.hpp>
#include <salem/graphics/controls/uiTextBox.hpp>
#include <salem/graphics/controls/uiContainer.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace Salem::Graphics {
Gui::Gui(const char *p_view_file) : m_view_file(p_view_file) {
  if (m_view_file != nullptr) { LoadView(); }
}

void Gui::on_mouse_down(int p_button, Physics::Position2d p_mouse_position) {
  // iterates through controls in reverse z-index order
  typedef std::map<std::int32_t, Graphics::Interface::InterfaceControl *>::iterator it_type;
  for (it_type it = m_interface_controls.end(); it != m_interface_controls.begin(); /* Do nothing */) {
    --it;
    it->second->on_mouse_down(p_button, p_mouse_position);
  }
}

void Gui::on_mouse_up(int p_button, Physics::Position2d p_mouse_position) {
  // iterates through controls in reverse z-index order
  typedef std::map<std::int32_t, Graphics::Interface::InterfaceControl *>::iterator it_type;
  for (it_type it = m_interface_controls.end(); it != m_interface_controls.begin(); /* Do nothing */) {
    --it;
    it->second->on_mouse_up(p_button, p_mouse_position);
  }
}

void Gui::on_mouse_move(Physics::Position2d p_mouse_position) {
  // iterates through controls in reverse z-index order
  typedef std::map<std::int32_t, Graphics::Interface::InterfaceControl *>::iterator it_type;
  for (it_type it = m_interface_controls.end(); it != m_interface_controls.begin(); /* Do nothing */) {
    --it;
    it->second->on_mouse_move(p_mouse_position);
  }
}

void Gui::on_char_input(unsigned int p_code_point, int p_mods) {
  // iterates through controls in reverse z-index order
  typedef std::map<std::int32_t, Graphics::Interface::InterfaceControl *>::iterator it_type;
  for (it_type it = m_interface_controls.end(); it != m_interface_controls.begin(); /* Do nothing */) {
    --it;
    it->second->on_char_input(p_code_point, p_mods);
  }
}

void Gui::on_key_down(int p_key) {
  // iterates through controls in reverse z-index order
  typedef std::map<std::int32_t, Graphics::Interface::InterfaceControl *>::iterator it_type;
  for (it_type it = m_interface_controls.end(); it != m_interface_controls.begin(); /* Do nothing */) {
    --it;
    it->second->on_key_down(p_key);
  }
}

void Gui::on_key_up(int p_key) {
  // iterates through controls in reverse z-index order
  typedef std::map<std::int32_t, Graphics::Interface::InterfaceControl *>::iterator it_type;
  for (it_type it = m_interface_controls.end(); it != m_interface_controls.begin(); /* Do nothing */) {
    --it;
    it->second->on_key_up(p_key);
  }
}

void Gui::LoadView() {
  try {
    std::ifstream jsonFile(m_view_file);

    boost::property_tree::ptree pt;
    boost::property_tree::read_json(m_view_file, pt);

    // Load Interface Fonts
    for (boost::property_tree::ptree::value_type &child : pt.get_child("interface.fonts")) {
      std::string font_id = child.second.get<std::string>("id");
      std::string font_source = child.second.get<std::string>("source");

      m_fonts[font_id] = new Font(font_source, GAMELOOP_PTR->GetShader("font"));
    }

    // Generate user interface objects
    for (boost::property_tree::ptree::value_type &child : pt.get_child("interface.objects")) {
      std::string object_type = child.second.get<std::string>("type");
      std::string object_id = child.second.get<std::string>("id");
      std::int32_t z_index = child.second.get<std::int32_t>("z-index", 0);

      Interface::InterfaceControl *control = CreateControl(object_type, object_id, child, z_index);
      if (control != nullptr) {
        AddUiControl(control, z_index);
      }
    }
  }
  catch (std::exception const &e) {
    std::cerr << e.what() << std::endl;
  }
}

Interface::InterfaceControl *Gui::CreateControl(
    std::string &p_object_type,
    std::string &p_object_id,
    boost::property_tree::ptree::value_type &p_control_data,
    int p_z_index
) {
  if (p_object_type == "button") {
    return GenerateControl<Controls::uiButton>(p_object_id, p_control_data, p_z_index);
  } else if (p_object_type == "image") {
    return GenerateControl<Controls::uiImage>(p_object_type, p_control_data, p_z_index);
  } else if (p_object_type == "textbox") {
    return GenerateControl<Controls::uiTextBox>(p_object_type, p_control_data, p_z_index);
  } else if (p_object_type == "container") {
    return GenerateControl<Controls::uiContainer>(p_object_type, p_control_data, p_z_index);
  } else {
    std::cout << "undefined control type" << std::endl;
    return nullptr;
  }
}

template<class T>
T *Gui::GenerateControl(
    std::string &p_object_id, boost::property_tree::ptree::value_type &p_control_data, int p_z_index
) {
  std::string shader = p_control_data.second.get<std::string>("shader", "ui");

  Controls::ControlAttributes attributes = LoadAttributes(p_control_data);
  auto textures = LoadObjectTextures(p_control_data);

  auto control = new T(p_object_id, LoadBoxTransform(p_control_data, p_z_index), shader, attributes);
  control->textures(textures);
  return control;
}

template<>
Controls::uiContainer *Gui::GenerateControl<Controls::uiContainer>(
    std::string &p_object_id, boost::property_tree::ptree::value_type &p_container_data, int p_z_index
) {
  std::string shader = p_container_data.second.get<std::string>("shader", "ui");
  Controls::ControlAttributes attributes = LoadAttributes(p_container_data);
  auto textures = LoadObjectTextures(p_container_data);

  auto control =
      new Controls::uiContainer(p_object_id, LoadBoxTransform(p_container_data, p_z_index), shader, attributes);
  control->textures(textures);
  LoadContainerChildren(p_container_data, control);
  return control;
}

void Gui::LoadContainerChildren(
    boost::property_tree::ptree::value_type &p_container_data, Controls::uiContainer *p_container) {
  for (boost::property_tree::ptree::value_type &child : p_container_data.second.get_child("children")) {
    std::string object_type = child.second.get<std::string>("type");
    std::string object_id = child.second.get<std::string>("id");
    std::int32_t z_index = child.second.get<std::int32_t>("z-index", 0);

    p_container->AddControl(CreateControl(object_type, object_id, child, z_index), z_index);
  }
}

Physics::Rectangle Gui::LoadBoxTransform(
    boost::property_tree::ptree::value_type &p_object_data, std::int32_t p_z_index) {
  double x = p_object_data.second.get<double>("position.x");
  double y = p_object_data.second.get<double>("position.y");
  double width = p_object_data.second.get<double>("size.width");
  double height = p_object_data.second.get<double>("size.height");

  return Physics::Rectangle(x, y, width, height);
}

std::vector<Objects::Texture> Gui::LoadObjectTextures(boost::property_tree::ptree::value_type &p_object_data) {
  auto texture_node = p_object_data.second.get_child_optional("textures");
  if (!texture_node) { return std::vector<Objects::Texture>(); }

  std::vector<Objects::Texture> textures;
  for (boost::property_tree::ptree::value_type &child : p_object_data.second.get_child("textures")) {
    std::string texture_path = child.second.get_value<std::string>();

    Objects::Texture texture;
    texture.id = Objects::TextureFromFile(texture_path);
    texture.type = "texture_ui";
    texture.path = texture_path;

    textures.push_back(texture);
  }

  return textures;
}

Controls::ControlAttributes Gui::LoadAttributes(boost::property_tree::ptree::value_type &p_object_data) {
  auto attribute_node = p_object_data.second.get_child_optional("attributes");
  if (!attribute_node) { return Controls::ControlAttributes(); }

  Controls::ControlAttributes attributes;
  std::string font_id = p_object_data.second.get<std::string>("attributes.font", "default");
  attributes.font = FindFontById(font_id);
  attributes.text_align = p_object_data.second.get<std::string>("attributes.text-align", "left");
  attributes.anchor = p_object_data.second.get<std::string>("attributes.anchor", "left");
  attributes.place_holder = p_object_data.second.get<std::string>("attributes.place-holder", "");
  std::string color_code = p_object_data.second.get<std::string>("attributes.font-color", "000000");
  attributes.font_color = ConvertColorCodeToVector3(color_code);
  attributes.font_size = p_object_data.second.get<float>("attributes.font-size", 1.0);
  attributes.is_password = p_object_data.second.get<bool>("attributes.is-password", false);
  attributes.label = p_object_data.second.get<std::string>("attributes.label", "");
  return attributes;
}

}