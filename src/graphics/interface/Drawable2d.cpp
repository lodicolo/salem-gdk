//
// Created by Dylan Dodds on 2/2/18.
//

#include <salem/graphics/interface/Drawable2d.hpp>
#include <salem/graphics/objects/Vertex.hpp>
#include <salem/graphics/tools/glCheckError.hpp>
#include <salem/system/GameLoop.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace Salem::Graphics::Interface {

Drawable2d::Drawable2d(Physics::Rectangle p_box_transform, std::string p_shader_name)
    : m_box_transform(p_box_transform),
      m_shader(GAMELOOP_PTR->GetShader(p_shader_name)),
      m_shader_name(p_shader_name) {}

void Drawable2d::Initialize() {
  Objects::Vertex vertex1;
  vertex1.Position = Physics::Vector3(box_transform().x, box_transform().y, 0.5);
  vertex1.TexCoords = Physics::Vector2(0.0f, 1.0f);
  vertex1.Normal = Physics::Vector3(1.0f, 1.0f, 1.0f);
  m_vertices.push_back(vertex1);

  Objects::Vertex vertex2;
  vertex2.Position = Physics::Vector3(box_transform().x + box_transform().width, box_transform().y, 0.5);
  vertex2.TexCoords = Physics::Vector2(1.0f, 1.0f);
  vertex2.Normal = Physics::Vector3(1.0f, 1.0f, 1.0f);
  m_vertices.push_back(vertex2);

  Objects::Vertex vertex3;
  vertex3.Position = Physics::Vector3(box_transform().x, box_transform().y + box_transform().height, 0.5);
  vertex3.TexCoords = Physics::Vector2(0.0f, 0.0f);
  vertex3.Normal = Physics::Vector3(1.0f, 1.0f, 1.0f);
  m_vertices.push_back(vertex3);

  Objects::Vertex vertex4;
  vertex4.Position = Physics::Vector3(
      box_transform().x + box_transform().width, box_transform().y + box_transform().height, 0.5);
  vertex4.TexCoords = Physics::Vector2(1.0, 0.0);
  vertex4.Normal = Physics::Vector3(1.0f, 1.0f, 1.0f);
  m_vertices.push_back(vertex4);

  // Triangle 1
  m_indices.push_back(0);
  m_indices.push_back(1);
  m_indices.push_back(2);

  // Triangle 2
  m_indices.push_back(1);
  m_indices.push_back(2);
  m_indices.push_back(3);

  glGenVertexArrays(1, &m_vao);
  glCheckError();

  glGenBuffers(1, &m_vbo);
  glCheckError();

  glGenBuffers(1, &m_ebo);
  glCheckError();

  glBindVertexArray(m_vao);
  glCheckError();

  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glCheckError();
  glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(Objects::Vertex), &m_vertices[0], GL_STATIC_DRAW);
  glCheckError();

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
  glCheckError();
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(unsigned int), &m_indices[0], GL_STATIC_DRAW);
  glCheckError();

  // vertex positions
  glEnableVertexAttribArray(0);
  glCheckError();
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Objects::Vertex), nullptr);
  glCheckError();

  // vertex normals
  glEnableVertexAttribArray(1);
  glCheckError();
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Objects::Vertex), (void *) offsetof(Objects::Vertex, Normal));
  glCheckError();

  // vertex TexCoords
  glEnableVertexAttribArray(2);
  glCheckError();
  glVertexAttribPointer(2,
                        2,
                        GL_FLOAT,
                        GL_FALSE,
                        sizeof(Objects::Vertex),
                        (void *) offsetof(Objects::Vertex, TexCoords));
  glCheckError();

  glBindVertexArray(0);
  glCheckError();
}

void Drawable2d::Draw() {
  unsigned int diffuseNr = 1;
  unsigned int specularNr = 1;

  m_shader->Use();
  glCheckError();

  for (unsigned int i = 0; i < m_textures.size(); i++) {
    glActiveTexture(GL_TEXTURE0 + i);
    glCheckError();

    std::string number;
    std::string name = m_textures[i].type;
    if (name == "texture_diffuse") {
      number = std::to_string(diffuseNr++);
      m_shader->SetInt(name + number, i);
    } else if (name == "texture_specular") {
      number = std::to_string(specularNr++);
      m_shader->SetInt(name + number, i);
    }

    glBindTexture(GL_TEXTURE_2D, m_textures[i].id);
    glCheckError();
  }

  glBindVertexArray(m_vao);
  glCheckError();

  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glCheckError();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
  glCheckError();

  glm::mat4 ortho_view =
      glm::ortho(0.0f, (float) GAMELOOP_PTR->view_port_width(), 0.0f, (float) GAMELOOP_PTR->view_port_height(), -1.0f, 1.0f);
  m_shader->SetMat4("projection", ortho_view);

  glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, nullptr);
  glCheckError();

  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glCheckError();
}
}