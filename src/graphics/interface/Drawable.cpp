//
// Created by ddodds on 10/30/17.
//

#include <salem/graphics/interface/Drawable.hpp>
#include <salem/system/GameLoop.hpp>
#include <salem/graphics/tools/glCheckError.hpp>

extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace Salem::Graphics::Interface {

Drawable::Drawable(Physics::Types::Transform p_transform, std::string p_shader_name, GLuint p_draw_method)
    : GameObject(p_transform),
      m_draw_method(p_draw_method),
      m_shader(GAMELOOP_PTR->GetShader(p_shader_name)),
      m_shader_name(p_shader_name) {}

void Drawable::Initialize() {
  glGenVertexArrays(1, &m_vao);
  glCheckError();

  glGenBuffers(1, &m_vbo);
  glCheckError();

  glGenBuffers(1, &m_ebo);
  glCheckError();

  glBindVertexArray(m_vao);
  glCheckError();

  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glCheckError();
  glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(Objects::Vertex), &m_vertices[0], GL_STATIC_DRAW);
  glCheckError();

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
  glCheckError();
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(unsigned int), &m_indices[0], GL_STATIC_DRAW);
  glCheckError();

  // vertex positions
  glEnableVertexAttribArray(0);
  glCheckError();
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Objects::Vertex), nullptr);
  glCheckError();

  // vertex normals
  glEnableVertexAttribArray(1);
  glCheckError();
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Objects::Vertex), (void *) offsetof(Objects::Vertex, Normal));
  glCheckError();

  // vertex TexCoords
  glEnableVertexAttribArray(2);
  glCheckError();
  glVertexAttribPointer(2,
                        2,
                        GL_FLOAT,
                        GL_FALSE,
                        sizeof(Objects::Vertex),
                        (void *) offsetof(Objects::Vertex, TexCoords));
  glCheckError();

  glBindVertexArray(0);
  glCheckError();

  GameObject::Initialize();
}

void Drawable::Draw() {
  unsigned int diffuseNr = 1;
  unsigned int specularNr = 1;

  m_shader->Use();
  glCheckError();

  for (unsigned int i = 0; i < m_textures.size(); i++) {
    glActiveTexture(GL_TEXTURE0 + i);
    glCheckError();

    std::string number;
    std::string name = m_textures[i].type;
    if (name == "texture_diffuse") {
      number = std::to_string(diffuseNr++);
      m_shader->SetInt(name + number, i);
    } else if (name == "texture_specular") {
      number = std::to_string(specularNr++);
      m_shader->SetInt(name + number, i);
    }

    glBindTexture(GL_TEXTURE_2D, m_textures[i].id);
    glCheckError();
  }

  glBindVertexArray(m_vao);
  glCheckError();

  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glCheckError();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
  glCheckError();

  Objects::Camera *camera = GAMELOOP_PTR->game_view()->camera();
  m_shader->SetMat4("model", transform().matrix_transform());
  m_shader->SetMat4("game_view", camera->transform().matrix_transform());
  m_shader->SetMat4("projection", camera->projection());

  glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, nullptr);
  glCheckError();

  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glCheckError();
}
}