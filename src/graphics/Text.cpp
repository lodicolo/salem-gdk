//
// Created by ddodds on 11/11/17.
//

#include <salem/graphics/Text.hpp>
#include <glad/glad.h>
#include <salem/system/GameLoop.hpp>
#include <salem/graphics/tools/glCheckError.hpp>
extern Salem::System::GameLoop *GAMELOOP_PTR;
namespace Salem::Graphics {
Text::Text(
    Physics::Rectangle p_box_transform,
    std::string p_text,
    Font &p_font,
    GLfloat p_scale,
    Physics::Vector3 p_color
) : Drawable2d(p_box_transform, "font"),
    m_font(p_font),
    m_scale(p_scale),
    m_text(p_text),
    m_color(p_color) {
  Initialize();
}

void Text::Draw() {
  shader()->Use();
  glUniform3f(glGetUniformLocation(shader()->id(), "textColor"), m_color.x, m_color.y, m_color.z);
  glCheckError();
  glActiveTexture(GL_TEXTURE0);
  glCheckError();
  glBindVertexArray(m_vao);
  glCheckError();

  float xOffset = (float) box_transform().x;

  for (std::string::const_iterator c = m_text.begin(); c != m_text.end(); ++c) {
    Character ch = m_font.characters()[*c];

    GLfloat xPosition = xOffset + ch.Bearing.x * m_scale;
    GLfloat yPosition = (float) box_transform().y - (ch.Size.y - ch.Bearing.y) * m_scale;

    GLfloat width = ch.Size.x * m_scale;
    GLfloat height = ch.Size.y * m_scale;

    GLfloat vertices[6][4] = {
        {xPosition, yPosition + height, 0.0, 0.0},
        {xPosition, yPosition, 0.0, 1.0},
        {xPosition + width, yPosition, 1.0, 1.0},

        {xPosition, yPosition + height, 0.0, 0.0},
        {xPosition + width, yPosition, 1.0, 1.0},
        {xPosition + width, yPosition + height, 1.0, 0.0}
    };

    glBindTexture(GL_TEXTURE_2D, ch.TextureID);
    glCheckError();
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glCheckError();
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glCheckError();

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glCheckError();

    xOffset += (ch.Advance >> 6) * m_scale;
  }

  glBindVertexArray(0);
  glCheckError();
  glBindTexture(GL_TEXTURE_2D, 0);
  glCheckError();
}

void Text::Initialize() {
  glGenVertexArrays(1, &m_vao);
  glCheckError();
  glGenBuffers(1, &m_vbo);
  glCheckError();

  glBindVertexArray(m_vao);
  glCheckError();
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glCheckError();

  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, nullptr, m_draw_method);
  glCheckError();

  glEnableVertexAttribArray(0);
  glCheckError();
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), nullptr);
  glCheckError();

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glCheckError();
  glBindVertexArray(0);
  glCheckError();
}
}