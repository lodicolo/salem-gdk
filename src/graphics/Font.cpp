//
// Created by ddodds on 11/11/17.
//


#include <salem/graphics/Font.hpp>
#include <ft2build.h>

#include FT_FREETYPE_H
#include <salem/graphics/Shader.hpp>
#include <iostream>
#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Salem::Graphics::Font::Font(std::string p_font_path, Shader *p_shader) {
  glm::mat4 projection = glm::ortho(0.0f, 800.0f, 0.0f, 600.0f);
  p_shader->Use();
  glUniformMatrix4fv(glGetUniformLocation(p_shader->id(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));

  FT_Library ft;
  if (FT_Init_FreeType(&ft)) {
    std::cout << "Error loading FreeType library" << std::endl;
  }

  FT_Face face;
  if (FT_New_Face(ft, p_font_path.c_str(), 0, &face)) {
    std::cout << "Error loading font;\n" << p_font_path << std::endl;
  }

  FT_Set_Pixel_Sizes(face, 0, 28);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

  for (GLubyte c = 0; c < 128; c++) {
    if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
      std::cout << "Failed to load a character glyph." << std::endl;
      continue;
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RED,
        face->glyph->bitmap.width,
        face->glyph->bitmap.rows,
        0,
        GL_RED,
        GL_UNSIGNED_BYTE,
        face->glyph->bitmap.buffer);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    Character character = {
        texture,
        glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
        glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
        static_cast<GLuint>(face->glyph->advance.x)
    };

    m_characters.insert(std::make_pair(c, character));
  }

  FT_Done_Face(face);
  FT_Done_FreeType(ft);
  std::cout << "Font loaded. " << std::endl;
}
