//
// Created by Dylan Dodds on 1/29/18.
//

#include <salem/graphics/objects/Camera.hpp>

namespace Salem::Graphics::Objects {

Camera::Camera(Physics::Types::Transform p_transform,
               float p_fov,
               float p_aspect_width,
               float p_aspect_height,
               float p_clip_near,
               float p_clip_far)
    : System::Objects::GameObject(p_transform),
      m_fov(p_fov),
      m_aspect_width(p_aspect_width),
      m_aspect_height(p_aspect_height),
      m_clip_near(p_clip_near),
      m_clip_far(p_clip_far) {}
}