//
// Created by ddodds on 1/17/18.
//

#include <string>
#include <salem/graphics/Shader.hpp>
#include <salem/graphics/objects/Mesh.hpp>

namespace Salem::Graphics::Objects {
Mesh::Mesh(
    Physics::Types::Transform &p_transform,
    std::string p_shader_name,
    std::vector<Vertex> p_vertices,
    std::vector<unsigned int> p_indices,
    std::vector<Texture> p_textures)
    : Drawable(p_transform, p_shader_name, GL_STATIC_DRAW) {
  Drawable::vertices(p_vertices);
  Drawable::indices(p_indices);
  Drawable::textures(p_textures);
}
}