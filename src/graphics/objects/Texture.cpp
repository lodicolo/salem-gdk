//
// Created by Dylan Dodds on 1/25/18.
//


#define STB_IMAGE_IMPLEMENTATION

#include <glad/glad.h>
#include <iostream>
#include <salem/graphics/objects/Texture.hpp>
#include <stb_image.h>
#include <salem/graphics/tools/glCheckError.hpp>

namespace Salem::Graphics::Objects {
unsigned int TextureFromFile(std::string &p_path) {
  unsigned int textureID;
  glGenTextures(1, &textureID);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, textureID);

  // set the texture wrapping/filtering options (on the currently bound texture object)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  int width, height, nrComponents;
  unsigned char *data = stbi_load(p_path.c_str(), &width, &height, &nrComponents, 0);
  if (data) {
    GLenum format;
    if (nrComponents == 1) {
      format = GL_RED;
    } else if (nrComponents == 3) {
      format = GL_RGB;
    } else if (nrComponents == 4) {
      format = GL_RGBA;
    }

    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
    glCheckError();
    glGenerateMipmap(GL_TEXTURE_2D);
    glCheckError();

    stbi_image_free(data);
  } else {
    std::cout << "Texture failed to load at path: " << p_path << std::endl;
    stbi_image_free(data);
  }

  return textureID;
}

unsigned int TextureFromFile(const char *p_path, const std::string &p_directory) {
  std::string filename = std::string(p_path);
  filename = p_directory + '/' + filename;

  return TextureFromFile(filename);
}
}