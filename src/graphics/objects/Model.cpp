//
// Created by ddodds on 1/17/18.
//

#include <salem/graphics/objects/Model.hpp>
#include <iostream>

namespace Salem::Graphics::Objects {
void Model::Draw() {
  for (unsigned int i = 0; i < m_meshes.size(); ++i) {
    m_meshes[i].Draw();
  }
}

void Model::Initialize() {
  Assimp::Importer importer;
  const aiScene
      *scene = importer.ReadFile(m_path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

  if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
    std::cout << "Error Loading Model: " << importer.GetErrorString() << std::endl;
    return;
  }

  m_directory = m_path.substr(0, m_path.find_last_of('/'));
  processNode(scene->mRootNode, scene);

  GameObject::Initialize();
}

void Model::processNode(aiNode *p_node, const aiScene *p_scene) {
  // process each mesh located at the current node
  for (unsigned int i = 0; i < p_node->mNumMeshes; i++) {
    aiMesh *mesh = p_scene->mMeshes[p_node->mMeshes[i]];
    m_meshes.push_back(processMesh(mesh, p_scene));
  }
  // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
  for (unsigned int i = 0; i < p_node->mNumChildren; i++) {
    processNode(p_node->mChildren[i], p_scene);
  }
}

std::vector<Texture>
Model::loadMaterialTextures(aiMaterial *p_material, aiTextureType p_textureType, std::string p_textureTypeName) {
  std::vector<Texture> textures;
  for (unsigned int i = 0; i < p_material->GetTextureCount(p_textureType); ++i) {
    aiString str;
    p_material->GetTexture(p_textureType, i, &str);
    bool skip = false;

    for (unsigned int j = 0; j < m_texturesLoaded.size(); ++j) {
      if (std::strcmp(m_texturesLoaded[j].path.data(), str.C_Str()) == 0) {
        textures.push_back(m_texturesLoaded[j]);
        skip = true;
        break;
      }
    }
    if (!skip) {
      Texture texture;
      texture.id = TextureFromFile(str.C_Str(), m_directory);
      texture.type = p_textureTypeName;
      texture.path = str.C_Str();
      textures.push_back(texture);
      m_texturesLoaded.push_back(texture);
    }
  }

  return textures;
}

Mesh Model::processMesh(aiMesh *p_mesh, const aiScene *p_scene) {
  std::vector<Vertex> vertices;
  std::vector<unsigned int> indices;
  std::vector<Texture> textures;

  // Process Vertices
  for (unsigned int i = 0; i < p_mesh->mNumVertices; ++i) {
    Vertex vertex;

    glm::vec3 position;
    glm::vec3 normal;

    position.x = p_mesh->mVertices[i].x;
    position.y = p_mesh->mVertices[i].y;
    position.z = p_mesh->mVertices[i].z;
    normal.x = p_mesh->mNormals[i].x;
    normal.y = p_mesh->mNormals[i].y;
    normal.z = p_mesh->mNormals[i].z;

    vertex.Position = position;
    vertex.Normal = normal;

    if (p_mesh->mTextureCoords[0]) {
      glm::vec2 texCoords;
      texCoords.x = p_mesh->mTextureCoords[0][i].x;
      texCoords.y = p_mesh->mTextureCoords[0][i].y;
      vertex.TexCoords = texCoords;
    } else {
      vertex.TexCoords = glm::vec2(0.0f, 0.0f);
    }

    vertices.push_back(vertex);
  }

  // Process Indices
  for (unsigned int i = 0; i < p_mesh->mNumFaces; i++) {
    aiFace face = p_mesh->mFaces[i];
    for (unsigned int j = 0; j < face.mNumIndices; j++) {
      indices.push_back(face.mIndices[j]);
    }
  }

  // Process Materials
  if (p_mesh->mMaterialIndex) {
    aiMaterial *material = p_scene->mMaterials[p_mesh->mMaterialIndex];
    std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
    textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

    std::vector<Texture> specularMaps = loadMaterialTextures(material,
                                                             aiTextureType_SPECULAR, "texture_specular");
    textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
  }

  Mesh mesh(transform(), shader_name(), vertices, indices, textures);
  mesh.Initialize();
  return mesh;
}

}