//
// Created by ddodds on 10/21/17.
//

#include <memory>

#include <salem/network/Packet.hpp>
#include <salem/network/PacketHeader.hpp>

namespace Salem::Network {
Salem::Network::Packet::Packet(PacketHeader p_packet_header,
                               char *p_data, size_t p_data_size)
    : m_packet_header(p_packet_header) {
  std::copy(m_packet_header.data(),
            m_packet_header.data() + PacketHeader::header_size,
            std::back_inserter(m_data));
  std::copy(p_data, p_data + p_data_size, std::back_inserter(m_data));
}
}

