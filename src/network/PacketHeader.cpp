//
// Created by ddodds on 10/21/17.
//

#include <cstring>
#include <iostream>

#include <salem/network/PacketHeader.hpp>

namespace Salem::Network {
PacketHeader::PacketHeader(char *p_data) {
  std::copy(p_data, p_data + PacketHeader::header_size, std::back_inserter(m_data));
}

PacketHeader::PacketHeader(PacketType p_packet_type, uint32_t p_data_size) {
  char cPacketType = (char) p_packet_type;
  std::copy(&cPacketType,
            &cPacketType + sizeof(char),
            std::back_inserter(m_data));

  char *dataSizePtr = reinterpret_cast<char *>(&p_data_size);
  std::copy(dataSizePtr,
            dataSizePtr + sizeof(uint32_t),
            std::back_inserter(m_data));
}
}
