//
// Created by ddodds on 10/21/17.
//

#include <salem/network/Client.hpp>

#include <iostream>
#include <salem/network/Packet.hpp>
#include <salem/network/PacketHeader.hpp>
#include <salem/network/tools/HelperFunctions.hpp>
#include <boost/asio.hpp>

namespace Salem::Network {
using boost::asio::ip::tcp;

Client::Client(boost::asio::ip::tcp::socket p_socket)
    : m_socket(std::move(p_socket)),
      m_read_buffer(PacketHeader::max_packet_size),
      m_temp_header_ptr(nullptr) {
  if (m_socket.is_open()) { BeginRead(); }
}

void Client::Connect(tcp::resolver::iterator p_endpoint_iterator) {
  boost::asio::async_connect(
      m_socket, p_endpoint_iterator,
      [this](boost::system::error_code ec, tcp::resolver::iterator) {
        if (!ec) {
          std::cout << "[Client] A new network connection has been established;" << std::endl;
          BeginRead();

          uint32_t message_size = 12;
          char *cMessageSize = reinterpret_cast<char *>(&message_size);
          char *data = reinterpret_cast<char *>(malloc(sizeof(uint32_t) + message_size));
          const char *message = "Hello World!";

          std::copy(cMessageSize, cMessageSize + sizeof(uint32_t), data);
          std::cout << data << std::endl;

          std::copy(message, message + message_size, data + sizeof(uint32_t));

          Packet packet(PacketHeader(PacketType::ping, message_size + sizeof(uint32_t)), data,
                        sizeof(uint32_t) + message_size);

          char *dataString = packet.body_data() + sizeof(uint32_t);
          std::cout << dataString << std::endl;
          std::cout << packet.GetHeader().GetBodySize() << std::endl;

          free(data);
          std::cout << dataString << std::endl;

          Send(packet);
        } else {
          std::cout << "connection failed" << std::endl;
          m_socket.close();
        }
      });
}

void Client::Send(Packet p_packet) {
  boost::asio::async_write(
      m_socket,
      boost::asio::buffer(p_packet.header_data(),
                          PacketHeader::header_size + p_packet.GetHeader().GetBodySize()),
      [this](boost::system::error_code ec, std::size_t size) {
        if (!ec) {
          std::cout << "Packet sent! size: " << size << std::endl;
        } else {
          std::cout << "Packet send failed!" << std::endl;
        }
      });
}

void Client::BeginRead() {
  boost::asio::async_read(
      m_socket,
      boost::asio::buffer(m_read_buffer.data(), PacketHeader::header_size),
      [this](boost::system::error_code ec, std::size_t) {
        if (!ec) {
          m_temp_header_ptr.reset(new PacketHeader(m_read_buffer.data()));

          if (!(m_temp_header_ptr->GetBodySize() > PacketHeader::max_packet_size)) {
            return BeginReadBody();
          }
        } else {
          m_socket.close();
        }

        BeginRead();
      });
}

void Client::BeginReadBody() {
  std::cout << m_temp_header_ptr->GetBodySize() << std::endl;
  boost::asio::async_read(
      m_socket,
      boost::asio::buffer(m_read_buffer.data() + PacketHeader::header_size, m_temp_header_ptr->GetBodySize()),
      [this](boost::system::error_code ec, std::size_t) {
        if (!ec) {
          std::cout << "bodysize: " << m_temp_header_ptr->GetBodySize() << std::endl;
          Packet packet(*m_temp_header_ptr,
                        m_read_buffer.data() + PacketHeader::header_size,
                        m_temp_header_ptr->GetBodySize());
          uint32_t offset = 0;
          uint32_t stringSize = *reinterpret_cast<uint32_t *>(packet.body_data());
          offset += sizeof(uint32_t);

          std::cout << stringSize << std::endl;
          char *stringData = packet.body_data() + offset;

          std::cout << stringData << std::endl;
        } else {
          m_socket.close();
        }

        BeginRead();
      });
}
}