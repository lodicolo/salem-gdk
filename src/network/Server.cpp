//
// Created by ddodds on 10/16/17.
//

#include <salem/network/Server.hpp>

#include <memory>
#include <boost/asio.hpp>

#include <iostream>

namespace Salem::Network {
using boost::asio::ip::tcp;

Server::Server(boost::asio::io_service &p_io_service, uint16_t p_port)
    : m_acceptor(p_io_service, boost::asio::ip::tcp::endpoint(tcp::v4(), p_port)),
      m_temp_socket(p_io_service),
      m_port(p_port) {
  std::cout << "[Server] Now listening for incoming connections" << std::endl;
  BeginAccept();
}

void Server::BeginAccept() {
  m_acceptor.async_accept(
      m_temp_socket,
      [this](boost::system::error_code ec) {
        if (!ec) {
          std::cout << "[Server] Accepted an incoming connection, <connection details>"
                    << std::endl;
          m_connections.emplace_back(new Client(std::move(m_temp_socket)));
        } else {
          std::cout << "[Server] Error accepting an incoming connection" << std::endl;
        }

        BeginAccept();
      });
}
}