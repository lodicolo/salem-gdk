//
// Created by ddodds on 10/31/17.
//

#ifndef TEST_CLIENT_GLOBALS_HPP
#define TEST_CLIENT_GLOBALS_HPP

#include <salem/system/GameLoop.hpp>

Salem::System::GameLoop *GAMELOOP_PTR(nullptr);

#endif //TEST_CLIENT_GLOBALS_HPP
