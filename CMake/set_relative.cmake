function(set_relative out root)

    set(relativeFiles "")
    foreach(file ${ARGN})
        list(APPEND relativeFiles "${root}/${file}")
    endforeach(file)
    set(${out} "${relativeFiles}" PARENT_SCOPE)

endfunction(set_relative)