macro(salem_add_library TARGET)

    cmake_parse_arguments(THIS "" "" "SOURCES;INCLUDES;COMPILE_FLAGS;DEPENDS;EXTERNAL_LIBS" ${ARGN})

    string(STRIP "${TARGET}" target)
    set(target "salem${target}")

    #    message(STATUS target=${target})
    #    message(STATUS "target_input=${target_input}")
    #    message(STATUS THIS_INCLUDES=${THIS_INCLUDES})
    #    message(STATUS THIS_EXTERNAL_LIBS=${THIS_EXTERNAL_LIBS})
    #    message(STATUS THIS_DEPENDS=${THIS_DEPENDS})
    #    message(STATUS THIS_COMPILE_FLAGS=${THIS_COMPILE_FLAGS})
    add_library(${target} ${THIS_SOURCES})

    string(REPLACE "-" "_" NAME_UPPER "${target}")
    string(TOUPPER "${NAME_UPPER}" NAME_UPPER)
    set_target_properties(${target} PROPERTIES DEFINE_SYMBOL ${NAME_UPPER}_EXPORTS)

    set_target_properties(${target} PROPERTIES DEBUG_POSTFIX d)
    set_target_properties(${target} PROPERTIES RELEASE_POSTFIX "")
    set_target_properties(${target} PROPERTIES MINSIZEREL_POSTFIX "")
    set_target_properties(${target} PROPERTIES RELWITHDEBINFO_POSTFIX "")

    if (NOT SALEM_PLATFORM_ANDROID)
        set_target_properties(${target} PROPERTIES SOVERSION ${VERSION_MAJOR}.${VERSION_MINOR})
        set_target_properties(${target} PROPERTIES VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}.${VERSION_BUILD})
    endif()

    target_include_directories(${target} PUBLIC ${PROJECT_SOURCE_DIR}/include)

    if (THIS_INCLUDES)
        target_include_directories(${target} PUBLIC ${THIS_INCLUDES})
    endif()

    if (CMAKE_BUILD_TYPE STREQUAL "Debug")
        target_compile_options(${target} PRIVATE -Wall)
    endif()

    if (SALEM_PLATFORM_MAC)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-std=c++1z>)
    elseif (SALEM_PLATFORM_WINDOWS)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-std=c++17>)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-fext-numeric-literals>)
    elseif (SALEM_PLATFORM_LINUX)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-std=c++17>)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-fext-numeric-literals>)
        target_link_libraries(${target} -lpthread)
    endif()

    # if using gcc >= 4.0 or clang >= 3.0 on a non-Windows platform, we must hide public symbols by default
    # (exported ones are explicitly marked)
    if(NOT SFML_OS_WINDOWS AND ((SFML_COMPILER_GCC AND NOT SFML_GCC_VERSION VERSION_LESS "4") OR (SFML_COMPILER_CLANG AND NOT SFML_CLANG_VERSION VERSION_LESS "3")))
        set_target_properties(${target} PROPERTIES COMPILE_FLAGS -fvisibility=hidden)
    endif()

    # Add specific compile flags
    if (THIS_COMPILE_FLAGS)
        target_compile_options(${target} PRIVATE ${THIS_COMPILE_FLAGS})
    endif()

    # Link Salem libraries
    if (THIS_DEPENDS)
        target_link_libraries(${target} ${THIS_DEPENDS})
    endif()

    # Link external libraries
    if (THIS_EXTERNAL_LIBS)
        target_link_libraries(${target} ${THIS_EXTERNAL_LIBS})
    endif()

    # TODO: Add an install rule

endmacro()