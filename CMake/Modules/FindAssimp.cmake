set(ASSIMP_FIND_ARCH 64)
if (ASSIMP_FIND_X86)
    set(ASSIMP_FIND_ARCH 32)
endif()

FIND_PATH(
        ASSIMP_INCLUDE_DIRS
        NAMES assimp/postprocess.h assimp/scene.h assimp/version.h assimp/config.h assimp/cimport.h
        PATHS
        /usr/local/include/
        C:\\MinGW\\include\\assimp\\
        $ENV{ASSIMP_ROOT_DIR}/include/
)

FIND_LIBRARY(
        ASSIMP_LIBRARIES
        NAMES assimp
        PATHS
        /usr/local/lib/
        /usr/local/lib${ASSIMP_FIND_ARCH}/
        C:\\MinGW\\lib\\
        $ENV{ASSIMP_ROOT_DIR}/lib/
        $ENV{ASSIMP_ROOT_DIR}/lib${ASSIMP_FIND_ARCH}/
)

IF (ASSIMP_INCLUDE_DIRS AND ASSIMP_LIBRARIES)
    SET(ASSIMP_FOUND TRUE)
ENDIF (ASSIMP_INCLUDE_DIRS AND ASSIMP_LIBRARIES)

IF (ASSIMP_FOUND)
    IF (NOT ASSIMP_FIND_QUIETLY)
        MESSAGE(STATUS "Found asset importer library: ${ASSIMP_LIBRARIES}")
    ENDIF (NOT ASSIMP_FIND_QUIETLY)
ELSE (ASSIMP_FOUND)
    IF (ASSIMP_FIND_REQUIRED)
        MESSAGE(FATAL_ERROR "Could not find asset importer library")
    ENDIF (ASSIMP_FIND_REQUIRED)
ENDIF (ASSIMP_FOUND)
