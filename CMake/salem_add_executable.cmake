macro(salem_add_executable TARGET)

    cmake_parse_arguments(THIS "GUI_APP" "RESOURCES_DIR" "SOURCES;INCLUDES;COMPILE_FLAGS;BUNDLE_RESOURCES;DEPENDS;EXTERNAL_LIBS" ${ARGN})

    string(STRIP "${TARGET}" target)
    source_group("${target}" FILES ${THIS_SOURCES})
    set(target "salem-${target}")

    set(target_input ${THIS_SOURCES})
    if (THIS_BUNDLE_RESOURCES)
        set(target_input ${target_input} ${THIS_BUNDLE_RESOURCES})
    endif()

    #    message(STATUS target=${target})
    #    message(STATUS "target_input=${target_input}")
    #    message(STATUS THIS_INCLUDES=${THIS_INCLUDES})
    #    message(STATUS THIS_EXTERNAL_LIBS=${THIS_EXTERNAL_LIBS})
    #    message(STATUS THIS_DEPENDS=${THIS_DEPENDS})
    #    message(STATUS THIS_COMPILE_FLAGS=${THIS_COMPILE_FLAGS})
    add_executable(${target} ${target_input})

    set_target_properties(${target} PROPERTIES DEBUG_POSTFIX d)
    set_target_properties(${target} PROPERTIES FOLDER "examples")

    if (THIS_INCLUDES)
        target_include_directories(${target} PUBLIC ${THIS_INCLUDES})
    endif()

    if (SALEM_PLATFORM_MAC)
        target_compile_options(${target} PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-std=c++1z>)
    elseif (SALEM_PLATFORM_WINDOWS)
        target_compile_options(${target} PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-std=c++17>)
        target_compile_options(${target} PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-fext-numeric-literals>)
    elseif (SALEM_PLATFORM_LINUX)
        target_compile_options(${target} PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-std=c++17>)
        target_compile_options(${target} PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-fext-numeric-literals>)
        target_link_libraries(${target} -lpthread)
    endif()

    # Add specific compile flags
    if (THIS_COMPILE_FLAGS)
        target_compile_options(${target} PRIVATE ${THIS_COMPILE_FLAGS})
    endif()

    # Link Salem libraries
    if (THIS_DEPENDS)
        target_link_libraries(${target} ${THIS_DEPENDS})
    endif()

    # Link external libraries
    if (THIS_EXTERNAL_LIBS)
        target_link_libraries(${target} ${THIS_EXTERNAL_LIBS})
    endif()

    # TODO: Resources

endmacro()